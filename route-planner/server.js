const express = require('express');
const OsmParser = require('./GIS/Parsers/OsmParser');
const CityGraph = require('./GIS/CityGraph');
const TransportGraph = require('./GIS/TransportGraph');
const cors = require('cors');
const bodyParser = require('body-parser');
const SearchEngine = require("./GIS/SearchEngine");
const dijkstra = require("./GIS/TransportGraph/dijkstra");
var util = require('util')
const DB = require("./GIS/MongoDB").getInstance()
const app = express();
const port = 3001;
const graph = new CityGraph.Graph();

let stops, // = require("./data/stops.json"),
	lines, // = require("./data/lines.json"),
	transportGraph = null;

//transportGraph = new TransportGraph.Graph(stops.features, lines.features, graph);

DB.stops.find({}).then(stops => {
		stops = stops.map(function (e, i) {
			e = e.toObject()
			if (!e.geometry.coordinates[0][0][0] || !e.geometry.coordinates[0][0][0] instanceof Number) {
				e.geometry.coordinates.push(e.geometry.coordinates[0]);
				e.geometry.coordinates = [e.geometry.coordinates];
			}
			return e;
		});

		//		stops = stops.filter(e => e.geometry.coordinates.length >= 4 || e.geometry.coordinates.length === 1);

		DB.lines.find({}).then(lines => {
			lines = lines.map(function (e) {
				return e.toObject()
			});
			lines = lines.filter(e => {
				return e.geometry.coordinates.length >= 1
			});
			transportGraph = new TransportGraph.Graph(stops, lines, graph);
		});
	})
	.catch(err => {
		console.error(err);
	});


OsmParser.parsePBF(graph, './data/damascus.roads.pbf', () => {
	console.log('finish parsing');
});

app.use(express.json());
app.use(express.static('public'));
app.use(cors());
app.use(bodyParser.json({
	limit: '100mb',
	extended: true
}));

app.get('/', (req, res) => res.sendFile(__dirname + '/public/index.html'));
app.get('/graph', (req, res) => res.sendFile(__dirname + '/public/graph.html'));
app.get('/graph-map', (req, res) => res.sendFile(__dirname + '/public/graph-map.html'));


app.get('/search', async (req, res) => {
	let {
		source_lat,
		destination_lat,
		source_lng,
		destination_lng,
		type,
		run,
		congestion
	} = req.query;

	let main_result = {};
	congestion = (congestion == "true")

	if (type == "run" || type == "car") {
		main_result = await graph.findPath(source_lat, source_lng, destination_lat, destination_lng, true, type, congestion);
	} else {
		main_result = await transportGraph.findPath(source_lat, source_lng, destination_lat, destination_lng, congestion);
	}

	if (run && type !== "run") {
		let runPath = await graph.findPath(source_lat, source_lng, destination_lat, destination_lng, true, type, congestion);
		return res.status(200).json([runPath, main_result]);
	} else {
		return res.status(200).json(main_result);
	}
});

app.get('/stop/:stopId', (req, res) => {
	const stopId = req.params.stopId;
	let lines = transportGraph.getStopLines(stopId);
	return res.json(lines);
});

app.get('/line/:lineId', (req, res) => {
	const lineId = req.params.lineId;
	let stops = transportGraph.getLineStops(lineId);
	return res.jsonp(stops);
});

app.listen(port, () => console.log(`Start listening on port ${port}!`));