const turf = require("@turf/turf");

class Node {
    constructor(lat, lon, id = 0) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.edges = [];
        this.reversed = [];
        this.visited = false;
        this.way = -1;
        this.geometry = turf.point([lon, lat]);
    }

    toString() {
        return `{"lat":${this.lat},"lon":${this.lon}` + (this.edges.length > 0 ? `,"edges":${this.edges.toString()}}` : `}`);
    }

    getKey() {
        return this.id;
    }

    getArray() {
        return [this.lon, this.lat];
    }
}

module.exports = Node;