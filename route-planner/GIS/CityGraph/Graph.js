var Node = require('./Node');
const turf = require("@turf/turf");
const {
    TreeMap
} = require('jstreemap');
const KDBush = require('kdbush');
const fs = require('fs');
const PriorityQueue = require('../../@trekhleb-javascript-algorithms/data-structures/priority-queue/PriorityQueue');
const Utilites = require("../Utilities");
const CongestionClient = require('../../CongestionClient');
const moment = require("moment");
const config = require("../../config");

class Graph {
    constructor() {
        this.nodes_map = new Map();
        this.ways = [];
    }

    addNode(node) {
        let node_id = parseInt(node.id);
        let node_obj = new Node(Number(node.lat), Number(node.lon), node_id);
        this.nodes_map.set(node_id, node_obj);
    }

    didFinishParse() {
        this.ways.forEach(way => {
            way.nodeRefs.forEach(node_id => {
                let node = this.nodes_map.get(parseInt(node_id))
                node.way = parseInt(way.id);
            });
        });
        this.nodes = [];
        for (var node of this.nodes_map.values()) {
            this.nodes.push(node);
        }
        this.index = new KDBush(this.nodes, p => p.lat, p => p.lon, 64, Float32Array);

    }

    DFS(current_node_id, destination_node_id, nodes_visited) {

        if (current_node_id === destination_node_id) {
            return nodes_visited.slice();
        }

        let current_node = this.nodes_map.get(current_node_id);

        if (current_node.visited === true) {
            return null;
        }

        current_node.visited = true;
        let val = null;


        if (current_node.edges.length > 1) {
            let destination_node = this.nodes_map.get(destination_node_id);
            current_node.edges = current_node.edges.sort((a, b) => {
                return Utilites.distanceBetweenNodes(this.nodes_map.get(a), destination_node) - Utilites.distanceBetweenNodes(this.nodes_map.get(b), destination_node);
            });
        }

        for (let i = 0; i < current_node.edges.length; i++) {
            nodes_visited.push(current_node.edges[i]);
            val = this.DFS(current_node.edges[i], destination_node_id, nodes_visited);

            if (val != null) {
                break;
            }

            nodes_visited.pop();
        }
        current_node.visited = false;
        return val;
    }

    findNearestNode(lat, lon, rad = 0.001) {
        let node = new Node(lat, lon),
            nearestNode = {},
            optimal_dist = Infinity,
            distance = 0,
            current = {};

        let nearest_nodes_id = this.index.within(lat, lon, rad);
        nearest_nodes_id.forEach((idx) => {
            current = this.nodes[idx];
            distance = Utilites.distanceBetweenNodes(node, current);
            if (optimal_dist > distance) {
                nearestNode = current.id;
                optimal_dist = distance;
            }
        });

        return nearestNode;
    }

    setEdges(nodeRefs, way) {
        for (let i = 0; i < nodeRefs.length - 1; i++) {
            this.nodes_map.get(parseInt(nodeRefs[i])).edges.push(parseInt(nodeRefs[i + 1]));
            if (way.tags.oneway && way.tags.oneway == "yes") {
                this.nodes_map.get(parseInt(nodeRefs[i + 1])).reversed.push(parseInt(nodeRefs[i]));
            } else {
                this.nodes_map.get(parseInt(nodeRefs[i + 1])).edges.push(parseInt(nodeRefs[i]));
            }
        }

        this.ways.push(way);
    }

    async dijkstra(startVertex, targetVertex, type = "run", congestion = false) {
        const distances = {};
        const visitedVertices = {};
        const previousVertices = {};
        const queue = new PriorityQueue();

        distances[startVertex.getKey()] = 0;
        queue.add(startVertex, distances[startVertex.getKey()]);

        const now = moment(config.DEFAULT_DATE);
        const weekday = now.weekday(),
            hour = now.hour();

        while (!queue.isEmpty()) {
            const currentVertex = queue.poll();

            if (currentVertex.getKey() === targetVertex.getKey()) {
                break;
            }

            let neighbors = currentVertex.edges;

            if (type === "run") {
                neighbors = neighbors.concat(currentVertex.reversed)
            }

            for (let i = 0; i < neighbors.length; i++) {
                let neighborId = neighbors[i];

                const neighbor = this.nodes_map.get(neighborId);

                if (!visitedVertices[neighbor.getKey()]) {

                    let heuristic = Utilites.distanceBetweenNodes(neighbor, targetVertex);

                    const existingDistanceToNeighbor = distances[neighbor.getKey()] ? distances[neighbor.getKey()] : Infinity;

                    const distanceToNeighborFromCurrent = distances[currentVertex.getKey()] + Utilites.distanceBetweenNodes(currentVertex, neighbor);

                    let timeToAchieveNeighbor = distanceToNeighborFromCurrent;


                    if (type == "car") {
                        try {
                            if (!congestion) {
                                timeToAchieveNeighbor = distanceToNeighborFromCurrent / config.CAR_MEDUIM_SPEED;
                                heuristic = heuristic / (config.CAR_MEDUIM_SPEED);
                            } else {
                                let turf_dist = turf.distance(turf.point(currentVertex.getArray()), turf.point(neighbor.getArray()));
                                timeToAchieveNeighbor = await CongestionClient.getInstance().getPredictedTimeWay(turf_dist, neighbor.way, weekday, hour);
                                heuristic = heuristic / (turf_dist / timeToAchieveNeighbor);
                            }
                        } catch (e) {
                            console.log(e.message);
                            timeToAchieveNeighbor = distanceToNeighborFromCurrent / config.CAR_MEDUIM_SPEED;
                            heuristic = heuristic / (config.CAR_MEDUIM_SPEED);
                            console.log('cong_exp', timeToAchieveNeighbor);
                        }
                    } else {
                        timeToAchieveNeighbor /= 4.5;
                        heuristic = heuristic / (4.5);
                    }

                    const neighborWeight = timeToAchieveNeighbor + heuristic;

                    if (timeToAchieveNeighbor < existingDistanceToNeighbor) {
                        distances[neighbor.getKey()] = timeToAchieveNeighbor;

                        if (queue.hasValue(neighbor)) {
                            queue.changePriority(neighbor, neighborWeight);
                        } else {
                            queue.add(neighbor, neighborWeight);
                        }

                        previousVertices[neighbor.getKey()] = currentVertex;
                    }
                }
            }
            visitedVertices[currentVertex.getKey()] = currentVertex;
        }
        return {
            dist: distances[targetVertex.getKey()],
            previousVertices,
        };
    }

    getVisArrayStrings() {
        let edges = 'var edges = [',
            nodes = 'var nodes = [',
            nodes_number = 0,
            edges_number = 0;

        this.nodes_map.forEach((value, key) => {
            nodes += (`{id: ${key}},`);
            nodes_number++;
            value.edges.forEach(e => {
                edges += (`{from: ${key}, to: ${e}},`);
                edges_number++;
            })
        });


        edges += ('];');
        nodes += ('];');

        return {
            edges,
            nodes,
            nodes_number,
            edges_number
        };
    }

    getCanvasArrayStrings() {
        let edges = 'var edges = [',
            nodes = 'var nodes = [',
            nodes_number = 0,
            edges_number = 0,
            dist_node;

        this.nodes_map.forEach((value, key) => {
            nodes += (`{lat: ${value.lat}, lng: ${value.lon}},`);
            nodes_number++;
            value.edges.forEach(e => {
                dist_node = this.nodes_map.get(e);
                edges += (`{from: [${value.lat}, ${value.lon}], to: [${dist_node.lat}, ${dist_node.lon}]},`);
                edges_number++;
            })
        });


        edges += ('];');
        nodes += ('];');

        return {
            edges,
            nodes,
            nodes_number,
            edges_number
        };
    }

    save(path = './data/graph.json') {
        fs.writeFile(path, this.nodes_map.toString(), function (err) {
            if (err) throw err;
            console.log('Graph has saved!');
        });
    }

    toString() {
        return this.nodes_map.toString();
    }

    getDijkstraPath(parentArray, current, path) {
        if (!parentArray[current]) {
            return;
        }

        this.getDijkstraPath(parentArray, parentArray[current].getKey(), path);

        path.push(current);
    }

    async findPath(src_lat, src_lng, dest_lat, dest_lng, geojson = false, type = "run", congestion = false) {
        let src = this.findNearestNode(Number(src_lat), Number(src_lng), 1);
        let dest = this.findNearestNode(Number(dest_lat), Number(dest_lng), 1);
        let res = await this.dijkstra(this.nodes_map.get(src), this.nodes_map.get(dest), type, congestion),
            path = [];
        this.getDijkstraPath(res.previousVertices, dest, path);
        if (!geojson) {
            return path.map(e => {
                let node = this.nodes_map.get(e);
                return {
                    lat: node.lat,
                    lng: node.lon
                }
            });
        } else {
            let points = path.map(e => {
                let node = this.nodes_map.get(e);
                return [
                    node.lon,
                    node.lat
                ]
            });
            let lines = {
                "type": "Feature",
                "properties": {
                    "name": type,
                    "type": 1
                },
                "geometry": {
                    "type": "LineString",
                    "coordinates": points
                }
            };

            let length = turf.length(lines),
                time = 0;

            if (type === "run") {
                time = length / 4.5;
            } else {
                time = length / 20;
            }

            lines.properties.length = length;
            lines.properties.time = time;

            let congestion_time = 0;

            if (type == "car" && !congestion) {
                congestion_time = await Utilites.getLineWithCongestion(this, lines);
            }

            return {
                lines: [lines],
                congestion_time,
                stops: [],
                path: [],
                start: {},
                end: {},
                cost: 0,
                length,
                time
            }
        }
    }
}

module.exports = Graph;