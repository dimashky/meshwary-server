const osmread = require('osm-read');
const not_carable = ["footway", "path", "steps", "bridleway", "services", "service"]
class OsmParser {

    static parsePBF(graph, filePath, callback) {
        osmread.parse({
            filePath,
            endDocument() {
                if (callback && typeof callback == "function") {
                    callback();
                }

                graph.didFinishParse();
            },
            node: function (node) {
                graph.addNode(node);
            },
            way: function (way) {
                let highway = way.tags.highway;
                if (highway && !not_carable.includes(highway))
                    graph.setEdges(way.nodeRefs, way);
            },
            relation: function (relation) {
                //    console.log('relation: ' + JSON.stringify(relation));
            },
            error: function (msg) {
                console.error('error: ' + msg);
            }
        });
    }
}

module.exports = OsmParser;