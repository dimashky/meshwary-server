const turf = require('@turf/turf');
const MAX_VALUE = 1000000;
const Congestion = require("../Congestion");
const moment = require("moment");
const config = require("../config");

class Utilities {
	static distance(coord1, coord2) {
		return turf.distance(coord1, coord2, {
			units: "kilometers"
		})
	}

	static getFirstStopContainsCoord(coord, stops) {
		let poly = {};
		for (let i = 0; i < stops.length; i++) {
			poly = stops[i];
			if (turf.booleanPointInPolygon(coord, poly)) {
				return i;
			}
		}
		return -1;
	}

	static getNearestStops(coord, stops_centers, excluded_stops, max_distance) {
		let result = [],
			stop,
			dist;

		for (let i = 0; i < stops_centers.length; i++) {
			if (excluded_stops.has(i)) {
				continue;
			}
			stop = stops_centers[i];
			dist = Utilities.distance(coord, stop);
			if (dist <= max_distance) {
				result.push(i);
			}
		}
		return result;
	}

	static stopCenter(stop) {
		return turf.centroid(stop);
	}

	static getStopsLines(stops, lines) {
		let stop_lines_map = new Map(),
			line_stops_map = new Map();

		stops.forEach((stop, stop_idx) => {
			// console.log("parse stop #" + stop_idx)
			lines.forEach((line, line_idx) => {
				let split;

				if (line.geometry.coordinates.length == 0)
					return;

				split = turf.lineSplit(line, stop);

				if (split.features.length === 0) {
					return;
				}

				if (!line_stops_map.get(line_idx)) {
					line_stops_map.set(line_idx, [stop_idx]);
				} else {
					line_stops_map.get(line_idx).push(stop_idx);
				}

				if (!stop_lines_map.get(stop_idx)) {
					stop_lines_map.set(stop_idx, [line_idx]);
				} else {
					stop_lines_map.get(stop_idx).push(line_idx);
				}
			});
		});
		return {
			stop_lines_map,
			line_stops_map
		};
	}

	static sortPoints(allStops, currentIdx, sortedStops) {
		if (currentIdx >= sortedStops.length - 1) {
			return;
		}
		let targetPoint = turf.point(allStops[sortedStops[currentIdx]].getArray());
		let stopsPoints = [];
		for (let i = currentIdx + 1; i < sortedStops.length; i++) {
			stopsPoints.push(turf.point(allStops[sortedStops[i]].getArray()));
		}
		let points = turf.featureCollection(stopsPoints);
		let nearest = turf.nearestPoint(targetPoint, points);
		let nearestIdx = nearest.properties.featureIndex + currentIdx + 1;
		let tmp = sortedStops[currentIdx + 1];
		sortedStops[currentIdx + 1] = sortedStops[nearestIdx];
		sortedStops[nearestIdx] = tmp;
		this.sortPoints(allStops, currentIdx + 1, sortedStops);
	}

	static replace(array, s1, s2) {
		let tmp = array[s1];
		array[s1] = array[s2];
		array[s2] = tmp;
	}

	static distanceBetweenNodes(node1, node2) {
		var from = turf.point([node1.lon, node1.lat]);
		var to = turf.point([node2.lon, node2.lat]);
		return turf.distance(from, to);
	}

	static normalize(val, max, min) {
		return (val - min) / (max - min);
	}

	static async getLineWithCongestion(citygraph, line) {
		let coords = line["geometry"]["coordinates"];
		let prev = null;
		let cost = 0;
		const now = moment(config.DEFAULT_DATE);
		const weekday = now.weekday(),
			hour = now.hour();
		for (let i = 0; i < coords.length; i++) {
			let node_id = citygraph.findNearestNode(coords[i][1], coords[i][0], 0.005);
			let node = citygraph.nodes_map.get(node_id);
			if (!node || !node.way) {
				continue;
			}
			if (!prev) {
				prev = node;
				continue;
			}
			let distance = turf.distance(node.geometry, prev.geometry),
				speed = await Congestion.getInstance().getWaysCongestion(hour, weekday, [node.way]);
			console.log(speed)
			cost += distance / (speed ? speed : config.CAR_MEDUIM_SPEED);
			prev = node;
		}
		return cost + 0.10;
	}
}

module.exports = Utilities;