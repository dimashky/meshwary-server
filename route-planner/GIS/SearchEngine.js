const Point = require("@turf/helpers").point;
const Utilities = require("./Utilities");
const {Routes, Route} = require("./Routes");
const fs = require("fs");

const sep = "======================\n";

class SearchEngine {
	constructor(lines, stops){
		this.visited_lines = new Set();
		this.visited_stops = new Set();
		const {stop_lines_map, line_stops_map} = Utilities.getStopsLines(stops, lines);
		this.stop_lines_map = stop_lines_map;
		this.line_stops_map = line_stops_map;
		this.lines = lines;
		this.stops = stops;
/*
		let str = "", str2 = "";
		this.stop_lines_map.forEach((v, k) => {
			str2 = "";
			v.forEach(e => str2 += " || "+ lines[e].properties.name);
			str += stops[k].properties.name + " ==> \n\t" + str2 + "\n"
		});
		fs.writeFile("stop_lines_map.log", str,'utf8', (err) => {});
		str = "", str2 = "";
		this.line_stops_map.forEach((v, k) => {
			str2 = "";
			v.forEach(e => str2 += " || "+ stops[e].properties.name);
			str += lines[k].properties.name + " ==> \n\t" + str2 + "\n"
		});
		fs.writeFile("line_stops_map.log", str,'utf8', (err) => {});
 */
		this.routes = new Routes(this.source, this.destination);
	}

	findPath(source_lat, source_lng, destination_lat, destination_lng){
		this.source = Point([source_lng, source_lat]);
		this.destination = Point([destination_lng, destination_lat]);
		this.visited_lines.clear();
		this.visited_stops.clear();
		this.routes = new Routes(this.source, this.destination);
		this.stringLog = `Start from ${source_lat},${source_lng} To ${destination_lat}, ${destination_lng}\n`;
		let route = new Route();
		let stopIdx = Utilities.getFirstStopContainsCoord(this.source, this.stops);
		this.search(this.source, route, stopIdx);
		fs.writeFile("search.log", this.stringLog,'utf8', (err) => {
			if (err) console.log(err);
			console.log("Successfully written SEARCH LOG to file.");
		});
		return this.routes.get();
	}

	search(position, route, stopIdx = -1){
		if(route.steps.length > 3){
			return;
		}

		this.stringLog += `Visiting stop ${(stopIdx > -1 ? this.stops[stopIdx].properties.name : "NONE")} with #${route.steps.length} steps\n`;

		if(Utilities.distance(position, this.destination) < 0.3){
			this.routes.add(route);
			this.stringLog += "Target distance is low store solution in position "+this.routes.length+"\n";
			return;
		}

		this.visited_stops.add(stopIdx);
		let nearest_stops = Utilities.getNearestStops(position, this.stops, this.visited_stops, 0.2);

		if(stopIdx === -1 && nearest_stops.length === 0){
			this.routes.add(route);
			this.stringLog += "No nearest stops, store solution in position "+this.routes.length+"\n";
			this.visited_stops.delete(stopIdx);
			return;
		}

		let lines = this.stop_lines_map.get(stopIdx), line_stops = [], next_position = {};

		if(lines && lines.length > 0){
			lines.forEach(line_idx => {
				line_stops = this.line_stops_map.get(line_idx);

				if(!line_stops || line_stops.size === 0){
					return;
				}

				this.visited_lines.add(line_idx);
				this.stringLog += `visit line name: ${this.lines[line_idx].properties.name}\n`;
				line_stops.forEach(stop_idx => {
					if(this.visited_stops.has(stop_idx)){
						return;
					}
					this.stringLog += `visit stop name: ${this.stops[stop_idx].properties.name}\n`;
					next_position = Utilities.stopCenter(this.stops[stop_idx]);
					route.addStep(this.stops[stop_idx].properties.name, this.lines[line_idx].properties.name, 50);
					this.search(next_position, route, stop_idx);
					this.stringLog += `UNDO visit stop name: ${this.stops[stop_idx].properties.name}\n`;
					route.undoStep();
				});
				this.stringLog += `UNDO visit line name: ${this.lines[line_idx].properties.name}\n`;
				this.visited_lines.delete(line_idx);
			});
		}
		this.stringLog += `visiting nearest stops count: ${nearest_stops.length}\n`;
		nearest_stops.forEach(stop_idx => {
			next_position = Utilities.stopCenter(this.stops[stop_idx]);
			route.addStep(this.stops[stop_idx].properties.name, "مشي", 0);
			this.stringLog += `visit nearest stop name: ${this.stops[stop_idx].properties.name}\n`;
			this.search(next_position, route, stop_idx);
			this.stringLog += `UNDO visit nearest stop name: ${this.stops[stop_idx].properties.name}\n`;
			route.undoStep();
		});
		this.visited_stops.delete(stopIdx);
		this.stringLog += `UNDO visit stop name: ${stopIdx > -1 ? this.stops[stopIdx].properties.name : ""}\n`;
	}
}

module.exports = SearchEngine;
