const mongoose = require('mongoose');


const StaticLine = {
    _instance: null,
    getInstance() {
        if (!this._instance) {
            mongoose.connect('mongodb://localhost:27017/meshwary', {
                useNewUrlParser: true
            });
            this._instance = {};
            this._instance.lines = mongoose.model('static_lines', new mongoose.Schema({
                any: {}
            }, {
                strict: true
            }));
            this._instance.stops = mongoose.model('stops', new mongoose.Schema({
                any: {}
            }));
        }
        return this._instance;
    }
};

module.exports = StaticLine;


/*let instance = null;
class MongoDB {

    static getInstance() {
        if(!instance) {
            instance = new MongoDB();
        }
        return instance;
    }

    async constructor() {
        this.db = null;
        const connection = `mongodb://${'127.0.0.1'}:${''}@${'localhost'}:${'27017'}/${'Meshwary'}`;
        console.log(connection)
        await mongoose.connect(connection, {poolSize: 10, useNewUrlParser: true})
            .then((db) => {
                this.db = db;
                console.log('DONE')
            }).catch(err => console.error.bind(console, 'MongoDB connection error', err));
        this.StaticLines = mongoose.model('static_lines', this.StaticLinesSchema());
    }

    StaticLinesSchema() {
        return ;
    }
}

module.exports = MongoDB;
*/