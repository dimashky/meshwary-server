class Line{
	constructor(source_stop_idx, destination_stop_idx, line_idx = -1, line_name = "مشي", weight = 0){
		this.source = source_stop_idx;
		this.destination = destination_stop_idx;
		this.idx = line_idx;
		this.name = line_name;
		this.weight = weight;
	}

	toString(){
		return `Source STOP: ${this.source}, Dest STOP: ${this.destination}, Name: ${this.name}`;
	}
}

module.exports = Line;
