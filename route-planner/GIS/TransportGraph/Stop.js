class Stop {
	constructor(center_lat, center_lng, name, idx) {
		this.lat = center_lat;
		this.lng = center_lng;
		this.lon = center_lng;
		this.name = name;
		this.idx = idx;
		this.edges = [];
		this.neareset_stops = [];
	}

	addEdge(line) {
		this.edges.push(line);
	}

	getArray() {
		return [this.lng, this.lat];
	}

	getKey() {
		return this.idx;
	}

	toString() {
		return "STOP: " + this.name;
	}

	getNeighbors() {
		return this.edges.map(e => e.destination);
	}

	getAdjacentVertices() {
		const adjacentVertices = {};

		this.getEdges().forEach(edge => {
			if (typeof (adjacentVertices[edge.destination]) === "undefined") {
				adjacentVertices[edge.destination] = [];
			}
			adjacentVertices[edge.destination].push(edge);
		});

		this.neareset_stops.forEach(stop => {
			if (typeof (adjacentVertices[stop]) === "undefined") {
				adjacentVertices[stop] = [];
			}
		});

		return adjacentVertices;
	}

	getEdges() {
		return this.edges;
	}
}

module.exports = Stop;