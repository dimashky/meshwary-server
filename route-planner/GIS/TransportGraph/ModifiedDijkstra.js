const logger = require("../../logger");
const turf = require("@turf/turf");
const CongestionClient = require('./../../CongestionClient');
const moment = require('moment');
const config = require("../../config");
const Utilities = require("../Utilities");
const fastpriorityqueue = require('fastpriorityqueue');



const transferPenalty = 6,
    walkingPenalty = 2,
    TRANSIT_SPEED = 15,
    WALKING_SPEED = 4.5;

function penaltyFunction(currentLines, adjacentLines) {
    let intersectedLines = [];
    let penalty = 0;

    if (currentLines && !currentLines.length) {
        currentLines = undefined;
    }

    if (!currentLines) {
        if (!adjacentLines.length) {
            penalty = walkingPenalty;
        } else {
            penalty = transferPenalty;
            intersectedLines = adjacentLines;
        }
    } else if (currentLines && adjacentLines.length) {
        let set = new Set();
        for (let i = 0; i < currentLines.length; i++) {
            set.add(currentLines[i].idx);
        }
        for (let i = 0; i < adjacentLines.length; i++) {
            if (set.has(adjacentLines[i].idx)) {
                intersectedLines.push(adjacentLines[i]);
            }
        }

        if (!intersectedLines.length) {
            penalty = transferPenalty;
            intersectedLines = adjacentLines;
        }
    }

    return {
        penalty,
        intersectedLines
    }
}

async function ModifiedDijkstra(graph, source, destination, destPoint, congestion = false) {

    const STOPS = graph.nodes,
        NUMNER_OF_STOPS = graph.nodes.length;
    const cost = Array(NUMNER_OF_STOPS).fill(Infinity),
        path = Array(NUMNER_OF_STOPS),
        lines = Array(NUMNER_OF_STOPS),
        queue = new fastpriorityqueue((a, b) => {
            return a.first <= b.first
        });

    const now = moment(config.DEFAULT_DATE);
    const weekday = now.weekday(),
        hour = now.hour();

    cost[source.getKey()] = 0;


    queue.add({
        first: 0,
        second: source.idx
    });

    console.time("start algo");
    while (!queue.isEmpty()) {
        let current_stop_idx = queue.poll().second,
            adjacentVertices = {},
            neighbors = [];
        const current_stop = STOPS[current_stop_idx];

        adjacentVertices = current_stop.getAdjacentVertices();
        neighbors = Object.keys(adjacentVertices);

        for (let i = 0; i < neighbors.length; i++) {
            let neighbor_idx = neighbors[i],
                neighbor = STOPS[neighbors[i]];

            let line, weight, legs = false,
                startPoint = turf.point(graph.nodes[current_stop_idx].getArray()),
                endPoint = turf.point(STOPS[neighbor_idx].getArray());

            let {
                penalty,
                intersectedLines
            } = penaltyFunction(lines[current_stop_idx], adjacentVertices[neighbor_idx]);

            if (!adjacentVertices[neighbor_idx].length) {
                weight = turf.distance(startPoint, endPoint)
                legs = true;
            } else {
                line = graph._lines[adjacentVertices[neighbor_idx][0].idx];
                line = turf.lineSlice(turf.nearestPointOnLine(line, startPoint), turf.nearestPointOnLine(line, endPoint), line);
                weight = adjacentVertices[neighbor_idx][0].weight;
            }

            if (congestion && !legs) {
                try {
                    weight = await CongestionClient.getInstance().getPredictedTimeAcrossLine(graph.cityGraph, line, weight, weekday, hour);
                } catch (e) {
                    console.log(e.message);
                    weight = weight / TRANSIT_SPEED;
                }
            } else {
                if (!legs) {
                    weight = weight / TRANSIT_SPEED;
                } else {
                    weight = weight / WALKING_SPEED;
                }
            }

            if (!weight) weight = 0;
            if (!penalty) penalty = 0;
            if (!cost[current_stop_idx]) cost[current_stop_idx] = 0;

            let alternativeCost = cost[current_stop_idx] + weight + penalty;

            if (alternativeCost < cost[neighbor_idx]) {
                cost[neighbor_idx] = alternativeCost;
                path[neighbor_idx] = (path[current_stop_idx] ? path[current_stop_idx] : []).concat(current_stop_idx);
                for (let i = 0; i < intersectedLines.length; i++) {
                    intersectedLines[i].time = weight;
                }
                lines[neighbor_idx] = intersectedLines;
                queue.add({
                    first: cost[neighbor_idx], // + heuristic,
                    second: neighbor_idx
                });
            }
        }
    }
    console.timeEnd("start algo");
    return {
        path,
        cost,
        lines
    };
};

module.exports = ModifiedDijkstra;