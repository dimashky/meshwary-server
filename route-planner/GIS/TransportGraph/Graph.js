const turf = require("@turf/turf");
const Utilities = require("../Utilities");
const Stop = require("./Stop");
const Line = require("./Line");
const dijkstra = require("./dijkstra");
const modifiedDijkstra = require("./ModifiedDijkstra");
const fs = require("fs");
const CongestionClient = require('./../../CongestionClient');

class Graph {
    constructor(stops, lines, cityGraph) {
        if (!Array.isArray(stops) || !Array.isArray(lines)) {
            console.log(stops, lines)
            this.stops = this.lines = [];
            throw new Error("Arguments invalid, stops and lines must be and Array");
        }
        CongestionClient.getInstance()
        this.lines = [];
        this.path_start_stop = {};
        this._lines = [...lines];
        this._stops = [...stops];
        this.cityGraph = cityGraph;
        const {
            line_stops_map,
            stop_lines_map
        } = Utilities.getStopsLines(stops, lines);
        this.nodes = new Array(stops.length);
        this.line_stops_map = line_stops_map;
        this.stop_lines_map = stop_lines_map;
        const empty_set = new Set();
        this.stops_centers = [];
        stops.forEach((stop, idx) => {
            this.stops_centers.push(turf.center(stop));
            try {
                let point = Utilities.stopCenter(stop);
                let lnglat = point['geometry'].coordinates;
                let created_stop = new Stop(lnglat[1], lnglat[0], stop['properties'].name, idx);
                this.nodes[idx] = created_stop;
            } catch (e) {

            }
        });
        this.nodes.forEach(node => {
            node.neareset_stops = Utilities.getNearestStops(turf.point(node.getArray()), this.stops_centers, new Set([node.getKey()]), 0.3)
        });
        lines.forEach((line, idx) => {

            this.lines.push({
                idx,
                name: line.properties.name
            });
            let line_stops = line_stops_map.get(idx);
            if (!line_stops)
                return;
            let targetPoint = turf.point(line['geometry'].coordinates[0]);
            line_stops.sort((a, b) => {
                let distA = turf.distance(targetPoint, turf.point(this.nodes[a].getArray()));
                let distB = turf.distance(targetPoint, turf.point(this.nodes[b].getArray()));
                return distA - distB;
            });
            Utilities.sortPoints(this.nodes, 0, line_stops);
            let node_idx = 0,
                next_node_idx = 0;
            for (let i = 0; i < line_stops.length - 1; i++) {
                node_idx = line_stops[i];
                next_node_idx = line_stops[i + 1];
                let distance = turf.distance(turf.point(this.nodes[node_idx].getArray()), turf.point(this.nodes[next_node_idx].getArray()));
                let created_line = new Line(node_idx, next_node_idx, idx, line['properties'].name, distance);

                this.nodes[node_idx].addEdge(created_line);
            }
        });
        // this.nodes.forEach((node, idx) => {
        // 	let point = turf.point(node.getArray());
        // 	let nearest_stops = Utilities.getNearestStops(point, stops, empty_set, 0.6);
        // 	nearest_stops.forEach(stop_idx => {
        // 		if (stop_idx === idx)
        // 			return;
        // 		let distance = turf.distance(point, turf.point(this.nodes[stop_idx].getArray()));
        // 		let created_line = new Line(idx, stop_idx, -1, "الأقدام", distance * 10);
        // 		this.nodes[idx].addEdge(created_line);
        // 	});
        // });
        //        this.saveGraphAsVisFiles();
        console.log("Transport Graph was loaded!");
    }

    async findPath(source_lat, source_lng, destination_lat, destination_lng, congestion = false) {
        let source_lnglat = [source_lng, source_lat];
        let dest_lnglat = [destination_lng, destination_lat];
        let nodes = this.nodes.map(node => turf.point(node.getArray()));
        nodes = nodes.filter(e => {
            return e;
        })
        let points = turf.featureCollection(nodes);
        let nearest_stop_from_source = turf.nearestPoint(turf.point(source_lnglat), points);
        let startNode = this.nodes[nearest_stop_from_source.properties.featureIndex];
        let nearest_stop_from_destination = turf.nearestPoint(turf.point(dest_lnglat), points);
        let endNodeIdx = nearest_stop_from_destination.properties.featureIndex;
        let endNode = this.nodes[endNodeIdx];

        let path = this.getPathModifiedDijkstra(await modifiedDijkstra(this, startNode, endNode, turf.point(dest_lnglat), congestion), startNode, endNode);
        let lines = [];
        let stops = [];
        let length = 0,
            cost = 0;
        let time = length / 20;
        let detailedPath = [];

        for (let i = 0; i < path.length; i++) {
            let edge = path[i];
            if (edge.from.idx !== startNode.idx) {
                detailedPath.push({
                    type: -1,
                    name: edge.from.name,
                    value: edge.from.name
                });
            }
            // console.log(edge);
            detailedPath.push({
                type: 1,
                name: edge.name,
                value: 'الذهاب بواسطة ' + edge.name
            });
            stops.push(edge.from);

            let startPoint = turf.point(edge.from.getArray()),
                endPoint = turf.point(edge.to.getArray()),
                line = {};

            if (edge.idx !== -1) {
                line = this._lines[edge.idx];
                line = turf.lineSlice(turf.nearestPointOnLine(line, startPoint), turf.nearestPointOnLine(line, endPoint), line);
                time += 0.16;
            } else {
                line = await this.cityGraph.findPath(startPoint.geometry.coordinates[1], startPoint.geometry.coordinates[0], endPoint.geometry.coordinates[1], endPoint.geometry.coordinates[0]);
                if (line.length > 1) {
                    line = turf.lineString(line.map(e => [e.lng, e.lat]), {
                        name: 'مشي',
                        type: -1
                    });
                    lines.push(line);
                } else {
                    line = null;
                }
            }

            if (!line) {
                continue;
            }

            let lineLength = turf.length(line);
            length += lineLength;
            line.properties.length = lineLength;
            if (line.properties.type === -1) {
                line.properties.time = lineLength / 4.5;
                line.properties.cost = 0;
            } else {
                line.properties.time = lineLength / 20;
                line.properties.cost = 50;
            }

            if (edge.time)
                time += edge.time;
            if (edge.idx !== -1) {
                cost += 50;
            }
            lines.push(line);
        }

        if (path.length !== 0) {
            stops.push(path[path.length - 1].to);
        }

        return {
            lines,
            stops,
            path: detailedPath,
            start: startNode,
            end: this.nodes[endNodeIdx],
            cost,
            length,
            time,
        };
    }

    getPathModifiedDijkstra(res, startNode, endNode) {
        let path = [],
            current = endNode.idx,
            prev = null;
        if (!res.path[endNode.idx]) {
            path.push({
                idx: -1,
                type: -1,
                name: 'مشي على الاقدام',
                to: endNode,
                from: startNode
            });
            return path;
        }
        res.path[endNode.idx].push(endNode.idx);

        res.path[endNode.idx].reverse().forEach(e => {
            current = e;
            if (current === startNode.idx) {
                path[path.length - 1].from = this.nodes[current];
                return;
            }
            if (!prev) {
                if (res.lines[current].length) {
                    prev = res.lines[current][0];
                    let temp = JSON.parse(JSON.stringify(prev));
                    temp.to = this.nodes[current];
                    if (path.length) {
                        path[path.length - 1].from = this.nodes[current];
                    }
                    path.push(temp);
                }
            } else {
                let exist = false;
                res.lines[current].forEach(e => {
                    if (!exist && e.idx === prev.idx) {
                        exist = true;
                        path[path.length - 1].time += e.time;
                    }
                });
                if (!exist) {
                    if (res.lines[current].length) {
                        prev = res.lines[current][0];
                        let temp = JSON.parse(JSON.stringify(prev));
                        temp.to = this.nodes[current];
                        path[path.length - 1].from = temp.to;
                        temp.type = 1;
                        temp.time = res.lines[current][0].time;
                        path.push(temp);
                    } else {
                        prev = null;
                        if (path.length) {
                            path[path.length - 1].from = this.nodes[current];
                        }
                        path.push({
                            idx: -1,
                            type: -1,
                            name: 'مشي على الاقدام',
                            to: this.nodes[current]
                        });
                    }
                }
            }
        });
        return path.reverse();
    }


    getPath(parentArray, current, prev, prevLine, path, stops, lines, prevEdges) {
        if (parentArray[current] === null) {
            if (prevLine) {
                //				path.push({type: -1, name: this.nodes[current].name, value:'🚏 ' + this.nodes[current].name});
                path.push({
                    type: 1,
                    name: prevLine,
                    value: "الذهاب بواسطة '" + prevLine + "'"
                });
                this.path_start_stop = this.nodes[current];
                stops.push(this.nodes[current]);
            }
            return;
        }

        let edge = {
            idx: -1,
            name: 'مشي'
        };
        if (prevEdges[current] !== -1) {
            edge = this.lines[prevEdges[current]];
        }
        // let edge = this.lines[prevEdges[current]]; //this.nodes[parentArray[current].getKey()].edges.find(e => e.destination === current);

        this.getPath(parentArray, parentArray[current].getKey(), current, edge.name, path, stops, lines, prevEdges);

        if (edge.name !== prevLine) {
            //	path.push({type: 0, name: edge.name, value: "⇙ توقف من '" + edge.name + "'"});
            path.push({
                type: -1,
                name: this.nodes[current].name,
                value: this.nodes[current].name
            });

            let start = stops[stops.length - 1],
                end = this.nodes[current],
                line = {};
            if (edge.idx > -1) {
                let startPoint = turf.point(start.getArray()),
                    endPoint = turf.point(end.getArray());
                line = this._lines[edge.idx];
                line = turf.lineSlice(turf.nearestPointOnLine(line, startPoint), turf.nearestPointOnLine(line, endPoint), line);
                lines.push(line);
            } else {
                line = this.cityGraph.findPath(start.lat, start.lng, end.lat, end.lng);
                if (line.length > 1) {
                    line = turf.lineString(line.map(e => [e.lng, e.lat]), {
                        name: 'مشي',
                        type: -1
                    });
                    lines.push(line);
                }
            }

            if (prevLine) {
                path.push({
                    type: 1,
                    name: prevLine,
                    value: "الذهاب بواسطة '" + prevLine + "'"
                });
                this.path_start_stop = this.nodes[current];
                stops.push(this.nodes[current]);
            } else {
                stops.push(this.nodes[current]);
            }
        }
    }

    getStopLines(stopId) {
        let idx = this._stops.findIndex(e => {
            if (e.id)
                return e.id == stopId
            else if (e._id)
                return e._id == stopId
        });
        let lines = this.stop_lines_map.get(idx);
        if (!lines) {
            return [];
        }
        let res = [];
        lines.forEach(lineIdx => {
            res.push(this._lines[lineIdx]);
        })
        return res;
    }

    getLineStops(lineId) {
        let idx = this._lines.findIndex(e => {
            if (e.id)
                return e.id == lineId
            else if (e._id)
                return e._id == lineId
        });
        let stops = this.line_stops_map.get(idx);
        if (!stops) {
            return [];
        }
        let res = [];
        stops.forEach(stopIdx => {
            res.push(this._stops[stopIdx]);
        })
        return res;
    }

    saveGraphAsVisFiles() {
        let node_str = "var nodes = [",
            edge_str = "var edges = [";
        this.nodes.forEach(node => {
            node_str += `{id:${node.getKey()}, label: '${node.name}', lat:${node.lat}, lng:${node.lng}},`;
            node.edges.forEach(e => {
                edge_str += `{from: ${e.source}, to:${e.destination} , label: '${e.name}', source: [${node.lat},${node.lng}], dest: [${this.nodes[e.destination].lat},${this.nodes[e.destination].lng}]},`
            });
        });
        node_str += "];";
        edge_str += "];";
        fs.writeFile("./public/data/nodes.js", node_str, 'utf8', () => {
        });
        fs.writeFile("./public/data/edges.js", edge_str, 'utf8', () => {
        });
    }
}


module.exports = Graph;