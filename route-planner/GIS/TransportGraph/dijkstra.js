const PriorityQueue = require('../../@trekhleb-javascript-algorithms/data-structures/priority-queue/PriorityQueue');
const Utilities = require('../Utilities');
const CongestionClient = require('../../CongestionClient');
const moment = require("moment");
const logger = require("../../logger")
const config = require("../../config");

module.exports = function dijkstra(graph, startNode) {

	const distances = {};
	const visitedVertices = {};
	const previousVertices = {};
	const prevEdges = {};
	const queue = new PriorityQueue();

	graph.nodes.forEach((node) => {
		distances[node.getKey()] = Infinity;
		previousVertices[node.getKey()] = null;
		prevEdges[node.getKey()] = null;
	});

	distances[startNode.getKey()] = 0;
	prevEdges[startNode.getKey()] = -1;
	queue.add(startNode, distances[startNode.getKey()]);

	const now = moment(config.DEFAULT_DATE);
	const weekday = now.weekday,
		hour = now.hour();

	let cnt = 0;

	while (!queue.isEmpty()) {
		const currentVertex = queue.poll();
		let edges = currentVertex.getEdges();

		edges.forEach((edge) => {

			let neighbor = graph.nodes[edge.destination];

			if (!visitedVertices[neighbor.getKey()]) {
				let weight = 0; // Utilities.normalize(edge.weight, 15, 0); // distance

				//const avg_speed = CongestionClient.getInstance().getPredictedPositionStatus(neighbor.lat, neighbor.lng, weekday, hour);
				//weight -= Utilities.normalize(avg_speed, 150, 0);

				if (prevEdges[currentVertex.getKey()] !== edge.idx || edge.idx == -1) {
					if (edge.idx === -1) {
						weight += 0;
					} else {
						weight += 50; // Utilities.normalize(50, 50, 0) + 5.0;
					}
				}

				if (prevEdges[currentVertex.getKey()] === edge.idx && edge.idx == -1) {
					weight += Infinity;
				}

				const existingDistanceToNeighbor = distances[neighbor.getKey()];
				const distanceToNeighborFromCurrent = distances[currentVertex.getKey()] + weight;

				if (distanceToNeighborFromCurrent < existingDistanceToNeighbor) {
					distances[neighbor.getKey()] = distanceToNeighborFromCurrent;

					if (queue.hasValue(neighbor)) {
						queue.changePriority(neighbor, distances[neighbor.getKey()]);
					}

					previousVertices[neighbor.getKey()] = currentVertex;
					prevEdges[neighbor.getKey()] = edge.idx;
				}

				if (!queue.hasValue(neighbor)) {
					queue.add(neighbor, distances[neighbor.getKey()]);
					prevEdges[neighbor.getKey()] = edge.idx;
				}
			}
		});

		visitedVertices[currentVertex.getKey()] = currentVertex;
	}

	return {
		distances,
		previousVertices,
		prevEdges
	};
}