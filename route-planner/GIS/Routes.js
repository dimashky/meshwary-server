class Routes{
	constructor(source, destination){
		this.source = source;
		this.destination = destination;
		this.solutions = [];
	}

	add(route){
		this.solutions.push(new Route([...route.steps], route.cost));
	}

	get(){
		this.solutions = this.solutions.sort((a,b) => a.compareTo(b));
		return this.solutions;
	}
}

class Route{
	constructor(steps = [], cost = 0) {
		this.steps = steps;
		this.cost = cost;
	}

	addStep(position, line, step_cost){
		this.steps.push({position, line});
		this.cost += step_cost;
	}

	undoStep(){
		this.steps.pop();
	}

	compareTo(route){
		return this.cost < route.cost;
	}
}

module.exports = {
	Routes,
	Route
};
