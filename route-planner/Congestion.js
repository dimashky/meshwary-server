const mysql = require('mysql2/promise');
let instance = null;
const moment = require('moment');
const config = require("./config");
const bluebird = require("bluebird");
const redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);

class Congestion {
	constructor() {
		console.time("open DB connection");
		mysql.createConnection({
				host: 'localhost',
				user: 'root',
				password: 'root',
				database: 'meshwary_congestion'
			})
			.then((conn) => {
				this.connection = conn;
				console.log("connected")
			});

		this.client = redis.createClient();
		this.client.on('error', err => console.log(err.message));
		this.init();
		console.timeEnd("open DB connection");
	}

	static getInstance() {
		if (!instance) {
			instance = new Congestion();
		}
		return instance;
	}

	async init() {
		let query = "";
		query = `SELECT (speed/read_count) as speed, way_id, start_hour as hour FROM congestion WHERE weekday = ${moment(config.DEFAULT_DATE).weekday()}`;

		let rows;
		if (!this.connection) {
			this.connection = await mysql.createConnection({
				host: 'localhost',
				user: 'root',
				password: 'root',
				database: 'meshwary_congestion'
			});
			console.log("connect DB");
		}
		try {
			rows = (await this.connection.execute(query))[0];
		} catch (e) {
			console.log(e.message);
			console.log(query);
		}
		for (let i = 0; i < rows.length; i++)
			this.client.set(rows[i].way_id + '_' + rows[i].hour, rows[i].speed);
	}

	async getWaysCongestion(hour, weekday, ways = null) {

		// let query = "";
		// if (hour && weekday) {
		// 	query = `SELECT AVG(speed/read_count) as avg FROM congestion WHERE weekday = ${weekday} AND start_hour <= ${hour} AND end_hour >= ${hour}`
		// } else if (hour) {
		// 	query = `SELECT AVG(speed/read_count) as avg FROM congestion WHERE start_hour <= ${hour} AND end_hour >= ${hour}`;
		// } else if (weekday) {
		// 	query = `SELECT AVG(speed/read_count) as avg FROM congestion WHERE weekday = ${weekday}`;
		// } else {
		// 	query = `SELECT AVG(speed/read_count) as avg FROM congestion WHERE start_hour = end_hour`;
		// }

		if (!ways || !ways.length) {
			return 0;
		}

		let avg = await this.client.getAsync(ways[0] + '_' + hour);
		return avg ? avg : 0;
	}
}

module.exports = Congestion;