const winston = require("winston")
const Path = require("path")

const logger = winston.createLogger({
	level: 'info',
	format: winston.format.json(),
	transports: [
		new winston.transports.File({
			filename: Path.join("logs", "error.log"),
			level: 'error'
		}),
		new winston.transports.File({
			filename: Path.join("logs", "info.log"),
			level: 'info'
		}),
		new winston.transports.File({
			filename: Path.join("logs", "debug.log"),
			level: 'debug'
		}),
		new winston.transports.File({
			filename: Path.join("logs", "combined.log"),
		})
	]
});

if (process.env.NODE_ENV !== 'production') {
	logger.add(new winston.transports.Console({
		format: winston.format.combine(
			winston.format.colorize(),
			winston.format.simple()
		)
	}));
}

module.exports = logger;