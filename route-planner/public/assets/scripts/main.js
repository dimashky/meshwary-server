var map = L.map('map').setView([33.5194844, 36.2781879], 15);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 22,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(map);

let srcMarker =L.marker(new L.LatLng(33.5194844, 36.2781879),{title: 'src', riseOnHover: true, draggable: true});
srcMarker.addTo(map);

let distMarker = L.marker(new L.LatLng(33.5244854, 36.2812879),{title: 'dist', riseOnHover: true, draggable: true});
distMarker.addTo(map);

var firstpolyline = new L.Polyline([], {
    color: 'red',
    weight: 3,
    opacity: 0.5,
    smoothFactor: 1
});
firstpolyline.addTo(map);

$('#srch-btn').on('click', () => {
    $.get('/rp?src_lat='+srcMarker.getLatLng().lat+
        '&src_lng='+srcMarker.getLatLng().lng+
        '&dist_lat='+distMarker.getLatLng().lat+
        '&dist_lng='+distMarker.getLatLng().lng, function(res){
            var pointList = [];
            for(let i = 0; i < res.length - 1; i++){
                var pointA = new L.LatLng(res[i].lat, res[i].lng);
                var pointB = new L.LatLng(res[i+1].lat, res[i+1].lng);
                pointList.push(pointA, pointB);    
            }
            firstpolyline.setLatLngs(pointList);
    })
});