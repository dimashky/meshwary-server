const util = require("util");
const Congestion = require("./Congestion");
const moment = require("moment");
const config = require("./config");
let instance = null;
const MEDIUM_SPEED = config.CAR_MEDUIM_SPEED;

class CongestionClient {
    constructor() {
        if (instance) {
            throw Error("call static method instead!");
        }
        const zerorpc = require("zerorpc");
        const client = new zerorpc.Client();
        try {
            client.connect("tcp://127.0.0.1:4242");
            client.invokeAsync = util.promisify(client.invoke);
        } catch (err) {
            console.log(err.message);
        }
        this.db = Congestion.getInstance();
        this.client = client;
        instance = this;
    }

    static getInstance() {
        if (!instance) {
            instance = new CongestionClient();
        }
        return instance;
    }

    getPredictedStreetStatus(streetName, weekDay, hour) {
        try {
            let avg_speed = this.client.invoke("getPredictedStreetStatus", streetName, weekDay, hour);
            return avg_speed > 0 ? avg_speed : MEDIUM_SPEED;
        } catch (e) {
            console.log(e);
            return MEDIUM_SPEED;
        }
    }

    async getPredictedTimeAcrossLine(citygraph, line, distance, weekDay, hour) {
        try {
            //            let avg_speed = await this.client.invokeAsync("getPredictedLineStatus", line, weekDay, hour);

            let coords = line["geometry"]["coordinates"];
            let ways = [];
            for (let i = 0; i < coords.length; i++) {
                let node_id = citygraph.findNearestNode(coords[i][1], coords[i][0], 0.005);
                let node = citygraph.nodes_map.get(node_id);
                if (node && node.way) {
                    ways.push(node.way);
                    break;
                }
            }

            let avg_speed = await this.db.getWaysCongestion(hour, weekDay, ways);

            return distance / (avg_speed > 0 ? avg_speed : MEDIUM_SPEED);
        } catch (e) {
            console.log(e);
            return distance / MEDIUM_SPEED;
        }
    }

    async getPredictedTimeWay(distance, way, weekDay, hour) {
        try {
            let avg_speed = await this.db.getWaysCongestion(hour, weekDay, [way]);
            return distance / (avg_speed > 0 ? avg_speed : MEDIUM_SPEED);
        } catch (e) {
            console.log(e);
            return distance / MEDIUM_SPEED;
        }
    }

    getPredictedPositionStatus(lat, lng, weekDay, hour) {
        try {
            let avg_speed = this.client.invokeAsync("getPredictedPositionStatus", lat, lng, weekDay, hour);
            return avg_speed > 0 ? avg_speed : MEDIUM_SPEED;
        } catch (e) {
            console.log(e);
            return MEDIUM_SPEED;
        }
    }
}

module.exports = CongestionClient