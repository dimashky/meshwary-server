# Meshwary - Server
This is the server of Meshary app. Meshwary is Mobility as a Service that provide a solution for public transports in cities.

## The Team behind Meshwary
* Mohamed Khair Dimashky
* Maher Al-Kassir
* Majd Al-Ajlani
* Mahmoud Mattar
* Moayad Khadem Al-Jamee
* Obai Sandouk (academic supervisor)
* Kinda Al-Tarboush (academic supervisor)
* Abd Al-Malek Moazen (business supervisor)