<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateGeojson extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string',
            'properties' => 'required',
            'properties.name' => 'required|string',
            'geometry' => 'required',
            'geometry.coordinates' => 'required|array',
            'geometry.type' => 'required|string'
        ];
    }
}
