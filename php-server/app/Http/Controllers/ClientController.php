<?php

namespace App\Http\Controllers;

use App\StaticLine;
use App\Stop;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function getBusLines(Request $request)
    {
        try {
            $data = StaticLine::where(function ($query) use ($request) {
                if ($request->search) {
                    $query->where('properties.name', 'like', '%' . preg_replace('/\s+/', '%', $request->search) . '%');
                }
            })->get()->toArray();
            for ($i = 0; $i < count($data); $i++) {
                if (count($data[$i]['geometry']['coordinates']) < 5) {
                    array_splice($data, $i, 1);
                }
            }
            for ($i = 0; $i < count($data); $i++) {
                $client = new Client();
                $rp_server_address = config('app.rp_server_address');
                $url = "http://$rp_server_address/line/" . $data[$i]['_id'];
                $response = $client->get($url);
                $response = json_decode((string)$response->getBody(), true);
                $data[$i]['properties']['stops'] = array_column($response, 'id');
            }
            return $this->response('success', $data);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function getBusStops(Request $request)
    {
        try {
            $data = Stop::where(function ($query) use ($request) {
                if ($request->search) {
                    $query->where('properties.name', 'like', '%' . preg_replace('/\s+/', '%', $request->search) . '%');
                }
            })->get();
            return $this->response('success', $data);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function getCongestions($weekday, $hour)
    {
        try {
            $client = new Client();
            $address = config('app.congestion_server_address');
            $url = "http://$address/congestion/$weekday/$hour";
            $response = $client->get($url);
            $response = json_decode((string)$response->getBody(), true);
            return $this->response('success', $response);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function getCorrelations($way_id)
    {
        try {
            $client = new Client();
            $address = config('app.congestion_server_address');
            $url = "http://$address/correlation/$way_id";
            $response = $client->get($url);
            $response = json_decode((string)$response->getBody(), true);
            return $this->response('success', $response);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function getWays()
    {
        try {
            $client = new Client();
            $address = config('app.congestion_server_address');
            $url = "http://$address/ways";
            $response = $client->get($url);
            $response = json_decode((string)$response->getBody(), true);
            return $this->response('success', $response);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }
}
