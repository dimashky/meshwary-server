<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;

class RoutePlanningController extends Controller
{
    public function rp(Request $request)
    {
        try {
            $client = new Client();
            $rp_server_address = config('app.rp_server_address');
            $url = '';
            if ($request->type) {
                $url = "&type=$request->type";
            }
            if ($request->run && $request->run === "true") {
                $url .= "&run=$request->run";
            }
            if ($request->congestion) {
                $url .= "&congestion=$request->congestion";
            }
            $url = "http://$rp_server_address/search?source_lat=$request->source_lat&source_lng=$request->source_lng&destination_lat=$request->destination_lat&destination_lng=$request->destination_lng" . $url;
            $response = $client->get($url);
            return $response;
        } catch (ConnectException $exception) { } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), $exception->getCode());
        }
    }
}
