<?php

namespace App\Http\Controllers;

use App\Trip;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TripController extends Controller
{
    public function index(Request $request)
    {
        try {
            $data = Trip::where('user_id', Auth::user()->id);
            if ($request->has('ongoing')) {
                $data = $data->whereNull('end_time');
            }
            return $this->response('success', $data->get());
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function show(Trip $trip)
    {
        try {
            return $this->response('success', $trip);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function start()
    {
        try {
            $data = Trip::start(Auth::user()->id);
            return $this->response('success', $data);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function end(Trip $trip)
    {
        try {
            if($trip->user_id != \Auth::user()->id) {
                throw new \Exception('bad request');
            }
            if($trip->end_time) {
                throw new \Exception('cannot end ended trip');
            }

            $trip->end_time = Carbon::now()->toDateTimeString();
            $trip->duration = Carbon::now()->diffForHumans($trip->start_time, 1);
            $trip->save();
            return $this->response('success', $trip);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }
}
