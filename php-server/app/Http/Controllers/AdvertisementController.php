<?php

namespace App\Http\Controllers;

use App\Advertisement;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $paginate = 10;
            if($request->paginate) $paginate = $request->paginate;
            return $this->response('success', Advertisement::paginate($paginate));
        } catch (\Exception $exception) {
            return $this->response('failed' . $exception->getMessage(), $exception->getTraceAsString(), 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $advertisement = Advertisement::create($request->all());
            return $this->response('success', $advertisement);
        } catch (\Exception $exception) {
            return $this->response('failed' . $exception->getMessage(), $exception->getTraceAsString(), 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertisement $advertisement)
    {
        try {
            $advertisement->update($request->all());
            $advertisement->save();
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed' . $exception->getMessage(), $exception->getTraceAsString(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertisement $advertisement)
    {
        try {
            $advertisement->delete();
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed' . $exception->getMessage(), $exception->getTraceAsString(), 400);
        }
    }
}
