<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	public function loginAPI(LoginRequest $request) {
        try {
            if(! Auth::attempt($request->only('email', 'password'))) {
                throw new \Exception("invalid email or password or your account is blocked");
            }
            $user = Auth::user();
            $user->accessToken = $user->createToken('Meshwary')->accessToken;
            return $this->response('success', $user);
        } catch (\Exception $e) {
            return $this->response('failed, '.$e->getMessage(), null, 400);
        }
    }

    public function login(LoginRequest $request) {

        try {

            if(! Auth::attempt($request->only('email', 'password'))) {
                throw new \Exception("invalid email or password or your account is blocked");
            }
            $user = Auth::user();
            $user->accessToken = $user->createToken('Meshwary')->accessToken;

            $cookie = \Cookie::forever('access_token', $user->accessToken);
        
            return redirect('/')->withCookie($cookie);
        } catch (\Exception $e) {
            return back()->with('message', $e->getMessage());
        }
    }

    public function logout() {
        Auth::logout();
        \Cookie::queue(\Cookie::forget('access_token'));
        \Cookie::queue(\Cookie::forget('laravel_session'));
        return redirect('/login');
    }
	
	public function logoutAPI(Request $request) {
		try {
            $accessToken = $request->user()->token();
            \DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                    'revoked' => true
                ]);
    
            $accessToken->revoke();

            return $this->response('success');
        } catch (\Exception $e) {
            return $this->response('failed', null, 400);
        }
    }
}
