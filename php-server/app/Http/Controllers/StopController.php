<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGeojson;
use App\Http\Requests\UpdateGeojson;
use App\Stop;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class StopController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->has('count')) {
                return $this->response('success', Stop::count());
            }

            $paginate = (int)$request->paginate ?? 10;

            $where = function ($query) use ($request) {
                if ($request->search) {
                    $query->where('name', 'like', '%' . preg_replace('/\s+/', '%', $request->search) . '%');
                }
            };

            $results = [
                "features" => Stop::where($where)->get(),
                "type" => "FeatureCollection"
            ];
            return $this->response('success', $results);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateGeojson $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGeojson $request)
    {
        try {
            $data = $request->validated();
            $static_line = Stop::create($data);
            return $this->response('success', $static_line);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Stop $stop
     * @return \Illuminate\Http\Response
     */
    public function show(Stop $stop)
    {
        try {
            return $this->response('success', $stop);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateGeojson $request
     * @param  Stop $stop
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGeojson $request, Stop $stop)
    {
        try {
            $stop->update($request->validated());
            return $this->response('success', $stop);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Stop $stop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stop $stop)
    {
        try {
            $stop->delete();
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function lines($id)
    {
        try {
            $client = new Client();
            $rp_server_address = config('app.rp_server_address');
            $url = "http://$rp_server_address/stop/$id";
            $response = $client->get($url);
            $response = json_decode((string)$response->getBody(), true);
            return $this->response('success', $response);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }
}
