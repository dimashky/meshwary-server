<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewStop;
use App\Http\Requests\StoreLine;
use App\Http\Requests\UpdateLine;
use App\Line;
use App\Services\LineOperation;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class LineController extends Controller
{
    private $lineOperation;

    public function __construct(LineOperation $lineOperation)
    {
        $this->lineOperation = $lineOperation;
    }

    /**
     * Display a listing of the resource.
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $paginate = 10;
            if ($request->paginate) {
                $paginate = (int)$request->paginate;
            }
            $where = function ($query) use ($request) {
                if ($request->search) {
                    $query->where('name', 'like', '%' . preg_replace('/\s+/', '%', $request->search) . '%');
                }
            };
            return $this->response('success', Line::where($where)->paginate($paginate, ['_id', 'name']));
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLine $request)
    {
        try {
            $data = $request->validated();
            $data['bus_id'] = \Auth::user()->id;
            $data['can_update'] = false;
            $line = Line::create($data);
            return $this->response('success', $line);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Line $line
     * @return \Illuminate\Http\Response
     */
    public function show(Line $line)
    {
        try {
            return $this->response('success', $line);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function stops($id)
    {
        try {
            $client = new Client();
            $rp_server_address = config('app.rp_server_address');
            $url = "http://$rp_server_address/line/$id";
            $response = $client->get($url);
            $response = json_decode((string)$response->getBody(), true);
            return $this->response('success', $response);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Line $line
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLine $request, Line $line)
    {
        try {
            $line->update($request->validated());
            return $this->response('success', $line);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Line $line
     * @return \Illuminate\Http\Response
     */
    public function destroy(Line $line)
    {
        try {
            $line->delete();
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function start(Request $request)
    {
        try {
            $user_id = \Auth::user()->id;
            Line::where('bus_id', $user_id)->where('can_update', true)->update(['can_update' => false]);
            Line::start(['bus_id' => $user_id, 'name' => $request->name]);
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function end()
    {
        try {
            $user_id = \Auth::user()->id;
            $line = Line::where('bus_id', $user_id)->where('can_update', true)->first();
            $line->update(['can_update' => false]);
            $this->lineOperation->lineToStaticLine($line);
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    public function newStopForRunningLine(NewStop $newStop)
    {
        try {
            $user_id = 1;//\Auth::user()->id;
            Line::where('bus_id', $user_id)->where('can_update', true)->push('stops', $newStop->validated());
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }
}
