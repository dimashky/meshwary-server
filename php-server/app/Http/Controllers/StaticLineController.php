<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGeojson;
use App\Http\Requests\UpdateGeojson;
use Illuminate\Http\Request;
use App\StaticLine;

class StaticLineController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $paginate = (int) $request->paginate ?? 10;

            $where = function ($query) use ($request) {
                if ($request->search) {
                    $query->where('name', 'like', '%' . preg_replace('/\s+/', '%', $request->search) . '%');
                }
            };

            $results = [
                "features" => StaticLine::where($where)->get(),
                "type" => "FeatureCollection"
            ];
            return $this->response('success', $results);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateGeojson $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGeojson $request)
    {
        try {
            $data = $request->validated();
            $static_line = StaticLine::create($data);
            return $this->response('success', $static_line);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  StaticLine $staticLine
     * @return \Illuminate\Http\Response
     */
    public function show(StaticLine $staticLine)
    {
        try {
            return $this->response('success', $staticLine);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateGeojson $request
     * @param  StaticLine $staticLine
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGeojson $request, StaticLine $staticLine)
    {
        try {
            $staticLine->update($request->validated());
            return $this->response('success', $staticLine);
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  StaticLine $staticLine
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaticLine $staticLine)
    {
        try {
            $staticLine->delete();
            return $this->response('success');
        } catch (\Exception $exception) {
            return $this->response('failed', $exception->getMessage(), 400);
        }
    }
}
