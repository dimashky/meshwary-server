<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Stop extends Model
{
    protected $connection = 'mongodb';

    protected $collection = 'stops';

    protected $guarded = [];

    public $timestamps = false;
}
