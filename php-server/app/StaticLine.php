<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class StaticLine extends Model
{
    protected $connection = 'mongodb';

    protected $collection = 'static_lines';

    protected $guarded = [];

    public $timestamps = false;
}
