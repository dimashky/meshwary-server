<?php

namespace App;

use Carbon\Carbon;
use Jenssegers\Mongodb\Eloquent\Model;

class Trip extends Model
{
    protected $connection = 'mongodb';

    protected $collection = 'trips';

    public static function start($user_id)
    {
        return Trip::create([
            'user_id' => $user_id,
            'route' => [],
            'distance' => 0.0,
            'avg' => 0.0,
            'start_time' => Carbon::now()->toDateTimeString(),
            'end_time' => null
        ]);
    }

    protected $guarded = [];
}
