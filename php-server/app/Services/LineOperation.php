<?php
/**
 * Created by PhpStorm.
 * User: maher
 * Date: 4/18/2019
 * Time: 3:56 PM
 */

namespace App\Services;


use App\Line;
use App\StaticLine;
use App\Stop;

class LineOperation
{
    public function lineToStaticLine(Line $line)
    {
        $points = $line->route;
        $stops = $line->stops;
        $dist_points = [];
        foreach ($points as $point) {
            array_push($dist_points, [$point['lng'], $point['lat']]);
        }
        $offset = 0.0001;
        foreach ($stops as $stop) {
            Stop::create([
                'type' => 'Feature',
                'properties' => ['name' => 'Stop'],
                'geometry' => ['coordinates' => [
                    [$stop['lng'] + $offset, $stop['lat'] - $offset],
                    [$stop['lng'] + $offset, $stop['lat'] + $offset],
                    [$stop['lng'] - $offset, $stop['lat'] + $offset],
                    [$stop['lng'] - $offset, $stop['lat'] - $offset]
                ], 'type' => 'Polygon']
            ]);
        }
        $static_line = StaticLine::create([
            'type' => 'Feature',
            'properties' => ['name' => $line->name],
            'geometry' => ['coordinates' => $dist_points, 'type' => 'LineString']
        ]);
        return $static_line;
    }
}