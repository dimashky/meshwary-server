<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Line extends Model
{
    protected $connection = 'mongodb';

    protected $collection = 'lines';

    protected $guarded = [];

    public static function start($data)
    {
        $all = array_merge($data, [
            'route' => [],
            'stops' => [],
            'can_update' => true
        ]);
        return Line::create($all);
    }
}
