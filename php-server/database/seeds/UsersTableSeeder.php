<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
			'name' => 'admin',
			'email' => 'admin@meshwary.com',
			'password' => bcrypt('secret')
		]);

        \App\User::create([
            'name' => 'majd',
            'email' => 'majd@meshwary.com',
            'password' => bcrypt('secret')
        ]);
		
		\App\User::create([
			'name' => 'maher',
			'email' => 'maher@meshwary.com',
			'password' => bcrypt('secret')
		]);
		
		\App\User::create([
			'name' => 'mahmoud',
			'email' => 'mahmoud@meshwary.com',
			'password' => bcrypt('secret')
		]);
		
		\App\User::create([
			'name' => 'medo',
			'email' => 'medo@meshwary.com',
			'password' => bcrypt('secret')
		]);

        \App\User::create([
            'name' => 'mohamad',
            'email' => 'mohamad@meshwary.com',
            'password' => bcrypt('secret')
        ]);
    }
}
