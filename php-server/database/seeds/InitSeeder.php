<?php

use Illuminate\Database\Seeder;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = \App\User::create([
            'name' => 'Test Account 1',
            'email' => 'test1@meshwary.com',
            'password' => bcrypt('secret')
        ]);


        $user2 = \App\User::create([
            'name' => 'Test Account 1',
            'email' => 'test2@meshwary.com',
            'password' => bcrypt('secret')
        ]);

        $bus_line1 = \App\BusLine::create(['name' => 'ميدان برزة']);
        $bus_line2 = \App\BusLine::create(['name' => 'مزة فيلات غربية']);

        $bus_line1->buses()->save($user1);
        $bus_line2->buses()->save($user2);

    }
}
