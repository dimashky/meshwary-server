<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinesCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Lines', function (Blueprint $table) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mongodb')->table('Lines', function (Blueprint $collection) {
            $collection->index('bus_id', 'INDEX_BUS_ID');
            $collection->string('name')->default('no given name');
            $collection->json('route')->default([]);
            $collection->boolean('can_update');
        });
    }
}
