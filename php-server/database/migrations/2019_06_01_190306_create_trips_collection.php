<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')
            ->create('trips', function (Blueprint $table) {
                $table->integer('user_id')->index();
                $table->json('route');
                $table->double('distance')->default(0);
                $table->double('avg')->default(0);
                $table->dateTime('start_time');
                $table->dateTime('end_time');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
