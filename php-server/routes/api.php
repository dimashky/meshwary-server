<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('real_time_data', function (\Illuminate\Http\Request $request) {
    \App\RealTime::create($request->all());
    return \Illuminate\Support\Facades\Response::json(['message' => 'success']);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('stop', 'StopController');
Route::apiResource('static_line', 'StaticLineController');

Route::get('bus_lines', 'ClientController@getBusLines');
Route::get('bus_stops', 'ClientController@getBusStops');

Route::get('rp', 'RoutePlanningController@rp');

Route::get('congestion/{weekday}/{hour}', 'ClientController@getCongestions');
Route::get('correlation/ways', 'ClientController@getWays');
Route::get('correlation/{way_id}', 'ClientController@getCorrelations');

Route::group(['middleware' => ['auth:api']], function () {

    Route::apiResource('line', 'LineController');
    Route::get('line/{id}/stops', 'LineController@stops');
    Route::get('stop/{id}/lines', 'StopController@lines');

    Route::post('line/start', 'LineController@start');
    Route::put('line/end', 'LineController@end');
    Route::post('line/stop', 'LineController@newStopForRunningLine');
    Route::post('logout', 'Auth\LoginController@logoutAPI');

    Route::get('trip/{trip}', 'TripController@show');
    Route::post('trip/start', 'TripController@start');
    Route::put('trip/{trip}/end', 'TripController@end');
    Route::get('trip', 'TripController@index');
});


Route::apiResource('line', 'LineController');

Route::post('login', 'Auth\LoginController@loginAPI');

Route::post('register', 'Auth\RegisterController@store');
