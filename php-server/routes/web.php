<?php

Route::view('/login', 'login')->name('login')->middleware('guest');

Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::view('/monitor', 'monitor')->where('all', '^(?!api).*$');
Route::view('{all}', 'home')->where('all', '^(?!api).*$')->name('home');