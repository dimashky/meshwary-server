import matplotlib.pyplot as plt
import numpy as np
from gmplot import gmplot
from app.Helper import openJsonFile, removeDuplicates, saveJsonFile
from leuvenmapmatching.matcher.newsonkrumm import NewsonKrummMatcher
from leuvenmapmatching.matcher.distance import DistanceMatcher
from app.Graph import Graph
from leuvenmapmatching import visualization as mmviz
from leuvenmapmatching.util.dist_latlon import interpolate_path
import leuvenmapmatching.visualization as mm_viz
import os
import pickle

gmap = gmplot.GoogleMapPlotter(33.5138, 36.2765, 13, "AIzaSyDUPt-N8xIRsKQIx_ui_SGolHRP2hQgVg4")
graph_path = os.path.join("data", "graph.dat")

g = Graph('./data/damascus.roads.pbf', False)


def drawOnMap(id, logs, states=None):
    lats, lons = zip(*[(log[0], log[1]) for log in logs])
    gmap.plot(lats, lons, 'cornflowerblue', edge_width=5)
    if(states):
        lats, lons = zip(*[(log[0], log[1]) for log in states])
        gmap.plot(lats, lons, '#FF0000', edge_width=5)
    gmap.draw("./testing/map_"+str(id)+".html")


def testMatcher(i, track, map_con, directory="testing"):
    global g
    print("Start test #{} for {}".format(i, len(track)))
    matcher = NewsonKrummMatcher(map_con,
                                 max_dist=100, max_dist_init=25,  # meter
                                 min_prob_norm=0.001,
                                 non_emitting_length_factor=0.75,
                                 obs_noise=20, obs_noise_ne=75,  # meter
                                 dist_noise=20,  # meter
                                 non_emitting_states=False)
    success = False
    interpolate_dd = 10
    states_pos = []
    while not success and interpolate_dd >= 0:
        try:
            track = interpolate_path(track, interpolate_dd)
            states, last_idx = matcher.match(track, unique=True)
            states_pos = [g.nodes[x]["location"] for pos in states for x in pos]
            drawOnMap(i, track, states_pos)
            mm_viz.plot_map(map_con, matcher=matcher, use_osm=False, show_graph=False,
                            filename=os.path.join("testing", "{}.png".format(i)))
            success = True
        except Exception as e:
            interpolate_dd -= 2
            print("{}, repeat for => {}".format(e, interpolate_dd))
    return states_pos

    print("End test #{}".format(i))


content = openJsonFile("./data/transportation/lines.json")
for idx, line in enumerate(content["features"]):
    coords = line["geometry"]["coordinates"]
    track = [(log[1], log[0]) for log in coords]
    res = removeDuplicates(testMatcher(idx, track, g.map_con))
    if(len(res) > 0):
        line["geometry"]["coordinates"] = res
saveJsonFile("./data/lines.json", content)
