import zerorpc
from CongestionServiceProvider import CongestionService
from flask import Flask, request, redirect, jsonify
from flask_cors import CORS
import multiprocessing

app = Flask(__name__)
service = CongestionService()


def runRPC():
	try:
		server = zerorpc.Server(service)
		server.bind("tcp://0.0.0.0:4242")
		server.run()
	except:
		print("Can not run ZeroRPC server, address 4242 in use!")

def runFlask():
    global app
    CORS(app)
    app.run(host='0.0.0.0', port=9000, debug=True)


@app.route("/congestion/<weekday>/<hour>", methods=["GET"])
def congestion(weekday, hour):
    global service
    return jsonify(service.getCongestionAsGeojson(weekday, hour))


@app.route("/correlation/<way_id>", methods=["GET"])
def correlation(way_id):
    global service
    return jsonify(service.getCorrelatedWays(way_id))


@app.route("/ways", methods=["GET"])
def ways():
    global service
    return jsonify(service.getWaysAsGeojson())


if __name__ == '__main__':
    p1 = multiprocessing.Process(target=runFlask)
    p2 = multiprocessing.Process(target=runRPC)

    p1.start()
    p2.start()
