from app.Graph import Graph
from app.Congestion import Congestion
from app.Correlation import Correlation
from app import WasilniParser
import io
import json
g = Graph('./data/osm/damascus.roads.pbf', fresh=False)

congestion = Congestion(g)
c = Correlation(g, congestion)
cong = c.calcCongestionMatrix(True, True)
corr = c.calcCorrelationMatrix(cong, 1, 0.1, 11, 14, True, True)
# c.parsePOI("./data/osm/damascus.POI.pbf")
#WasilniParser.saveIntoGPX('./data/wasilni', 'booking_summary')
#WasilniParser.parse(g, "./data/booking_summary.json", "./data/output.log", "./data/congestion.json")
#congestion.extractCongestionFromGPX("./data/wasilni/booking_summary 03-19.gpx", hourly=True)
