from app.Graph import Graph
from app import WasilniParser
from app import Helper
from app.Congestion import Congestion
from app.Correlation import Correlation
from app.Helper import getColorBasedOnSpeed
from app import Matcher
import dateutil
import redis
r = redis.Redis(host='localhost', port=6379, db=0)


class CongestionService(object):
    def __init__(self):
        self.graph = Graph('./data/osm/damascus.roads.pbf', fresh=False)
        self.congestion = Congestion(self.graph)
        self.correlation = Correlation(self.graph, self.congestion)
        print("Congestion Service Provider is Ready!")

    def getPredictedStreetStatus(self, streetName, weekDay, hour):
        return Helper.findAvgSpeed(self.congestion, streetName, weekDay, hour)

    def getPredictedLineStatus(self, line, weekDay, hour):
        coords = line["geometry"]["coordinates"]
        nodes = [self.graph.getNearestNode(coord[1], coord[0])[1] for coord in coords]
        ways = set([node.get("way", -1) for node in nodes])
        avg = self.congestion.getWaysAvgSpeed(list(ways), weekDay, hour)
        return avg

    def getPredictedPositionStatus(self, lat, lng, weekDay, hour):
        node = self.graph.getNearestNode(lat, lng)[1]
        if not "way" in node.keys():
            return -1
        streetName = self.graph.ways[node["way"]]["name"]
        return Helper.findAvgSpeed(self.congestion, streetName, weekDay, hour)

    def pushTrackingLogs(self, tracks, match_type="distance"):
        matched_tracks = []
        congestion_dict = {}
        last_time = None
        redis_dict = {}
        for track in tracks:
            if(match_type == "hmm"):
                logs = Matcher.hmmMatcher(self.graph, track)
            else:
                logs = Matcher.distanceMatcher(self.graph, track)
            matched_logs = []
            for log in logs:
                lat, lng = self.graph.nodes.get(log[0])["location"]
                time = log[1]
                last_time = time
                matched_logs.append((lat, lng, time))

            dateObj = dateutil.parser.parse(last_time)
            weekday = dateObj.date().weekday()
            hour = dateObj.time().hour
            matched_tracks.append(matched_logs)
            congestion_dict_tmp = self.congestion.detect(logs)
            redis_key = ""
            for way, (speed, cnt) in congestion_dict_tmp.items():
                record = congestion_dict.get(way, (0, 0))
                congestion_dict[way] = (speed + record[0], cnt + record[1])
                redis_key = str(way)+"_"+str(hour)
                prev_speed = redis_dict.get(redis_key)

        ways_id = [c for c in congestion_dict.keys() if congestion_dict[c][1] > 1]
        historical = self.congestion.sqllite.getWaysCongestion(hour=hour, weekday=weekday, ways=ways_id)
        avg_speed = {}
        for h in historical:
            way_id, start_hour, speed, read_count = h
            avg_speed[way_id] = speed/read_count

        props = []
        for w, (s, c) in congestion_dict.items():
            hist_speed = avg_speed.get(w, -1)
            current_speed = (s/c)
            total_speed = hist_speed * 0.7 + current_speed * 0.3
            props.append({"historical": hist_speed, "speed": s/c, "color": getColorBasedOnSpeed(total_speed)})
            if(speed > 5 and speed < 100):
                r.set(str(w)+"_"+str(hour), total_speed)
        ways = self.graph.getWaysAsGeojson(ways_id, props)
        return {
            "congestion": ways,
            "matched_tracks": matched_tracks
        }

    def getCongestionAsGeojson(self, weekday, hour):
        return self.congestion.getCongestionAsGeojson(weekday, hour)

    def getWaysAsGeojson(self):
        ways = self.correlation.getWays()
        return self.graph.getWaysAsGeojson(ways)

    def getCorrelatedWays(self, way_id):
        correlated_ways = self.correlation.getCorrelatedWays(way_id)
        return self.graph.getWaysAsGeojson([c[0] for c in correlated_ways], [{"confidence": c[1]} for c in correlated_ways])
