import ast
import io
import os
import json
import glob
import math
import dateutil
import gpxpy
import pandas as pd
from pathlib import Path
from datetime import datetime
from haversine import haversine
from app.Helper import convertArabicDate, findAndSetAvgSpeed, getNearesLocation


def saveIntoGPX(excelPath, sheetname):
    excelPaths = [excelPath]
    if os.path.isdir(excelPath):
        excelPaths = glob.glob(os.path.join(excelPath, "*.xlsx"))

    for excelPath in excelPaths:
        gpx = gpxpy.gpx.GPX()
        print("Processing => " + excelPath)
        df = pd.read_excel(excelPath, sheetname)
        for i in range(len(df)):
            print("Parsing #"+str(i+1))
            gpx_track = gpxpy.gpx.GPXTrack()
            gpx.tracks.append(gpx_track)
            gpx_segment = gpxpy.gpx.GPXTrackSegment()
            gpx_track.segments.append(gpx_segment)
            try:
                logs = df['Logs'][i]
                logs = ast.literal_eval(logs)
                if logs[0]['date'] == "????-??-?? ??:??:??":
                    continue
                for log in logs:
                    date = convertArabicDate(log['date'])
                    date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                    gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(log['lat'], log['lng'], time=date))
            except Exception as e:
                print(e)
        path = os.path.realpath(excelPath)
        filename = Path(excelPath).stem
        file = open(os.path.join(os.path.dirname(path), "{}.gpx".format(filename)), "w")
        file.write(gpx.to_xml())
        file.close()


def parse(graph, tripsFilePath, outputPath, congestionPath):
    file = open(tripsFilePath, "r")
    trips = json.load(file)
    avg = []
    with io.open(outputPath, "w", encoding="utf-8") as out:
        for i, trip in enumerate(trips):
            if(len(trip["logs"]) == 0):
                continue
            last = {"id": -1}
            avgSpeed = 0
            totalNodes = 0
            date = convertArabicDate(trip["logs"][0]["date"])
            dateObj = dateutil.parser.parse(date)
            weekday = dateObj.date().weekday()
            out.write("#%5d الانطلاق: %15s الوجهة: %15s يوم الأسبوع: %d \n" % (i, trip["from"], trip["to"], weekday))
            lastLogIdx = len(trip["logs"]) - 1
            for logIdx, log in enumerate(trip["logs"]):
                ID = log["id"]
                NODE = graph.nodes[ID]
                if not "way" in NODE.keys():
                    continue
                date = convertArabicDate(log["date"])
                location = (log['lat'], log['lng'])
                dateObj = dateutil.parser.parse(date)
                weekday = dateObj.date().weekday()
                streetName = graph.ways[NODE["way"]]["name"]
                out.write("\t"+streetName+"\tالساعه:"+str(dateObj.time().hour) + " الدقيقة:"+str(dateObj.time().minute)+"\n")

                if last["id"] != -1:
                    lastLocation = last["location"]
                    lastTime = last["time"]
                    rdelta = dateutil.relativedelta.relativedelta(dateObj, lastTime)
                    h = rdelta.hours + (rdelta.minutes/60) + (rdelta.seconds/(60*60))
                    km = haversine(location, lastLocation)
                    if(h > 0 and km > 0):
                        avgSpeed += km/h
                        totalNodes += 1

                    if(last["id"] != streetName or logIdx == lastLogIdx):
                        if(totalNodes > 2):
                            findAndSetAvgSpeed(avg, last["id"], weekday, lastTime.hour, avgSpeed, totalNodes)
                            avgSpeed /= (totalNodes + 0.00000000001)
                            out.write("\t\t==> السرعه الوسطية لتجاوز شارع %s هي %f Km/h \n" % (last["id"], avgSpeed))
                        # save
                        avgSpeed = 0
                        totalNodes = 0

                last["id"] = streetName
                last["time"] = dateObj
                last["location"] = location
    file.close()
    with io.open(congestionPath, "w", encoding="utf-8") as out:
        out.write(json.dumps(avg))
