
import requests
import math
from app.Helper import getNearesLocation
from leuvenmapmatching.matcher.newsonkrumm import NewsonKrummMatcher
from leuvenmapmatching.util.dist_latlon import interpolate_path


def mapboxMatcher(locations, N=0):
    mapbox_url = "https://api.mapbox.com/matching/v5/mapbox/driving/"
    result = set()
    chuncks = [locations]
    logs_number = len(locations)
    if(logs_number > 100):
        chuncks = []
        chuncks_number = math.ceil(logs_number/(100-N))
        for i in range(chuncks_number):
            req_locs = [locations[locationIdx] for locationIdx in range(i*(100-N), (i+1)*(100-N)) if(locationIdx < logs_number)]
            chuncks.append(req_locs)

    for chunck in chuncks:
        loc_str = ""
        for loc in chunck:
            loc_str += ("%f,%f;" % (loc['lng'], loc['lat']))
        loc_str = loc_str[:-1]
        request_url = mapbox_url + loc_str + "?tidy=true&steps=false&access_token=pk.eyJ1IjoiZGltYXNoa3kiLCJhIjoiY2pmNTdmNWR4MTQyczMybDNoMjk5azdpMiJ9.xx_HwmYVzB9g6woNfUF00A"
        res = (requests.get(request_url))
        if(not res.status_code == 200):
            print("Failed Mapbox %d" % res.status_code)
            return []
        res = res.json()
        if(res["code"] == "Ok"):
            result |= set([(tracepoint["location"][1], tracepoint["location"][0]) for tracepoint in res["tracepoints"] if tracepoint != None])
        else:
            print("ERROR in mapbox api => %s" % (res["code"]))
    return list(result)


# logs = [(lat, lng, time)]
def hmmMatcher(graph, logs):
    matcher = NewsonKrummMatcher(graph.map_con,
                                 max_dist=50, max_dist_init=25,  # meter
                                 min_prob_norm=0.001,
                                 non_emitting_length_factor=0.75,
                                 obs_noise=50, obs_noise_ne=75,  # meter
                                 dist_noise=50,  # meter
                                 non_emitting_states=False)
    success = False
    interpolate_dd = 12
    states = []
    track = [(log[0], log[1]) for log in logs]
    turn = True
    _states = []

    while not success and interpolate_dd > 5:
        try:
            if(interpolate_dd < 12):
                track = interpolate_path(track, interpolate_dd)
            states, last_idx = matcher.match(track, unique=True)
            success = True
        except Exception as e:
            interpolate_dd -= 2
            print("{}, repeat for interpolate factor => {}".format(e, interpolate_dd))

    if not success:
        return distanceMatcher(graph, logs)

    for state in states:
        if turn:
            idx = getNearesLocation(graph.nodes[state[0]]["location"], logs)
            _states.append((state[0], logs[idx][2]))
        else:
            idx = getNearesLocation(graph.nodes[state[1]]["location"], logs)
            _states.append((state[1], logs[idx][2]))
        turn = not turn

    return _states


def distanceMatcher(graph, logs):
    states = []
    for lat, lng, time in logs:
        node_id, node = graph.getNearestNode(lat, lng)
        states.append((node_id, time))
    return states
