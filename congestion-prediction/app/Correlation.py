from osmread import parse_file, Way, Node
import pandas as pd
from tkinter import *
from pandastable import Table, TableModel
from app.Helper import saveJsonFile, openJsonFile
from app.Helper import distanceBetweenTwoWays
import numpy as np
import os.path


class Correlation:
    def __init__(self, graph, congestion):
        self.congestion = congestion
        self.graph = graph

    def parsePOI(self, pbf_file):
        for entity in parse_file(pbf_file):
            if isinstance(entity, Node) and 'name' in entity.tags:
                node_id, node = self.graph.getNearestNode(entity.lat, entity.lon)
                way_id = node.get("way")
                way = self.graph.ways.get(way_id)
                if(way):
                    if "amenity" in entity.tags:
                        way["poi"].append((entity.tags["name"], entity.tags["amenity"]))
                    if "shop" in entity.tags:
                        way["poi"].append((entity.tags["name"], entity.tags["shop"]))
                    if "tourism" in entity.tags:
                        way["poi"].append((entity.tags["name"], entity.tags["tourism"]))
                    self.graph.ways[way_id] = way
        self.graph.update()

    def calcCongestionMatrix(self, store=False, show=False):
        records = self.congestion.sqllite.getWaysCongestion()
        results = [[] for i in range(0, 24)]
        way_dict = {}
        for (way, hour, speed, count) in records:
            value = way_dict.get(way, [(0, 0) for i in range(0, 24)])
            value[hour] = (value[hour][0] + speed, value[hour][1] + count)
            way_dict[way] = value

        for way in way_dict:
            for h in range(0, 24):
                value = way_dict[way]
                speed = value[h][0]/(value[h][1] + 0.000001)
                results[h].append(speed)
        data = pd.DataFrame(results).transpose()
        if(store):
            saveJsonFile("./data/congestionMatrix.json", data.to_json())
        if(show):
            showTable(data).mainloop()
        return data

    def calcCorrelationMatrix(self, congestion_matrix, time, distance, start_time, end_time, store=True, show=False):
        c_row, c_col = congestion_matrix.shape
        R = np.zeros((c_row, c_row))
        CC = np.zeros((c_row, c_row))
        pre_calc = np.zeros((c_row))
        way_keys = list(self.graph.ways.keys())

        for i in range(c_row):
            for j in range(c_col):
                pre_calc[i] += congestion_matrix.loc[i, j] <= 30

        for j in range(start_time, end_time):
            cv = np.zeros((c_row))
            is_found = False
            for i in range(c_row):
                if congestion_matrix.loc[i, j] <= 30:
                    if is_found == False:
                        for k in range(c_row):
                            for t in range(j+1, j + time + 1):
                                if congestion_matrix.loc[k, t] <= 30:
                                    cv[k] = 1
                                    break
                        is_found = True
                    R[i] = np.add(R[i], cv)
        correlated_ways = {}
        for i in range(c_row):
            for k in range(c_row):
                dist = distanceBetweenTwoWays(self.graph, way_keys[i], way_keys[k])
                if dist > distance or i == k:
                    R[i][k] = 0
                else:
                    CC[i][k] = R[i][k] / (pre_calc[i] + 0.0000001)
                    if(CC[i][k] > 0.15):
                        item = correlated_ways.get(way_keys[i], list())
                        if not item:
                            correlated_ways[way_keys[i]] = []
                        correlated_ways[way_keys[i]].append((way_keys[k], round(CC[i][k], 4)))
        r_data = pd.DataFrame(R)
        cc_data = pd.DataFrame(CC)
        if(store):
            saveJsonFile("./data/correlationMatrix.json", r_data.to_json())
            saveJsonFile("./data/CorrelationConfidence.json", cc_data.to_json())
            saveJsonFile("./data/correlated_ways.json", correlated_ways)
        if(show):
            showTable(r_data).mainloop()
            showTable(cc_data).mainloop()
        return (r_data, cc_data)

    def getCorrelatedWays(self, way_id):
        fname = "./data/correlated_ways.json"
        correlation_dict = openJsonFile(fname)
        correlated = correlation_dict.get(way_id, [])
        return correlated

    def getWays(self):
        fname = "./data/correlated_ways.json"
        C = openJsonFile(fname)
        return list(C.keys())


class showTable(Frame):
    def __init__(self, df, parent=None):
        self.parent = parent
        Frame.__init__(self)
        self.main = self.master
        self.main.geometry('800x800+200+100')
        self.main.title('Table')
        f = Frame(self.main)
        f.pack(fill=BOTH, expand=1)
        self.table = pt = Table(f, dataframe=df,
                                showtoolbar=True, showstatusbar=True)
        pt.show()
        return
