import io, json
from haversine import haversine

def convertArabicDate(date):
    eastern_to_western = {"٠":"0","١":"1","٢":"2","٣":"3","٤":"4","٥":"5","٦":"6","٧":"7","٨":"8","٩":"9"}
    newDate = ""
    for idx,char in enumerate(date):
        if(char in eastern_to_western.keys()):
            newDate += eastern_to_western[char]
        else:
            newDate += date[idx]
    return newDate

def dayPeriod(hour):
    if(hour < 8):
        return (0, 8)
    if(hour >= 8 and hour < 11):
        return (8,11)
    if(hour >= 11 and hour < 15):
        return (11,15)
    if(hour >= 15 and hour < 19):
        return (15,19)
    if(hour >= 19 and hour <= 23):
        return (19,23)

# (WAY_NAME | WEEKDAY | MIN_TIME | MAX_TIME | TOTAL_SPEED | LOG_COUNT)
def findAndSetAvgSpeed(avgList, wayName, dayweek, hour, speed, count):
    period = dayPeriod(hour)
    foundIdx = -1
    for idx, el in enumerate(avgList):
        if el[0] == wayName and el[1] == dayweek and hour >= el[2] and hour < el[3]:
            foundIdx = idx
            break
    
    if(foundIdx > -1):
        el = avgList[foundIdx]
        newEl = (el[0], el[1], el[2], el[3], el[4]+speed, el[5]+count)
        avgList[foundIdx] = newEl
        return
    
    el = (wayName, dayweek, period[0], period[1], speed, count)
    avgList.append(el)
    return


def findAvgSpeed(avgList, wayName, dayweek, hour):
    foundIdx = -1
    for idx, el in enumerate(avgList):
        if el[0] == wayName and el[1] == dayweek and hour >= el[2] and hour < el[3]:
            foundIdx = idx
            break

    if(foundIdx > -1):
        el = avgList[foundIdx]
        return el[4]/el[5]

    return -1
    
def exportWays(graph, path):
    with io.open(path, "w", encoding="utf-8") as out:
        features = []
        for way in graph.ways.items():
            coordinates = [graph.nodes[i]["location"] for i in way[1]["nodes"]]
            feature = {
                "type": "Feature",
                "properties": {"name": way[1]["name"]},
                "geometry": {
                        "type": "LineString",
                        "coordinates": coordinates
                }
            }
            features.append(feature)
        out.write(json.dumps({"type": "FeatureCollection","features": features}))
        
def getNearesLocation(lat_lng, arr_lat_lng):
    MIN_DISTANCE = 10000000
    ID = 1
    lat = lat_lng[0]
    lng = lat_lng[1]
    for idx, value in enumerate(arr_lat_lng):
        node_lat = value[0]
        node_lng = value[1]
        distance = haversine((lat, lng), (node_lat,node_lng))
        if distance < MIN_DISTANCE:
            ID = idx
            MIN_DISTANCE = distance
    return ID


def openJsonFile(filePath):
    file = open(filePath, mode="r", encoding="utf-8")
    content = json.load(file)
    file.close()
    return content

def saveJsonFile(filePath, content):
    file = open(filePath, "w")
    json.dump(content, file)
    file.close()
    return content


def removeDuplicates(duplicate):
    final_list = [] 
    for num in duplicate: 
        if num not in final_list: 
            final_list.append(num) 
    return final_list 

def distanceBetweenTwoWays(graph, way1, way2):
    way1 = graph.ways.get(way1)
    way2 = graph.ways.get(way2)
    if not way1 or not way2:
        return 1000000
    node1 = graph.nodes.get(way1.get("nodes")[0], {"location": (0,0)})
    node2 = graph.nodes.get(way2.get("nodes")[0], {"location": (0,0)})
    return haversine(node1.get("location", (0,0)), node2.get("location", (0,0)))
    
def getColorBasedOnSpeed(speed):
    if(speed <= 15):
        color = '#e32636'
    elif(speed <= 25):
        color = '#ff8000'
    elif(speed <= 35):
        color = '#e3d326'
    elif(speed <= 50):
        color = '#75ec6a'
    else:
        color = '#22a716'
    return color
