import sqlite3


class SqlLiteRepo:
    def __init__(self, sql_file_path):
        self.sqlpath = sql_file_path
        db = sqlite3.connect(self.sqlpath, check_same_thread=False)
        cursor = db.cursor()
        cursor.execute('''
			CREATE TABLE IF NOT EXISTS congestion(
											id INTEGER PRIMARY KEY AUTOINCREMENT,
											way_id UNSIGNED BIG INTEGER,
											start_hour INTEGER,
											end_hour INTEGER,
											weekday INTEGER,
											speed FLOAT,
											read_count INTEGER)
		''')
        cursor.execute("CREATE INDEX IF NOT EXISTS WAY_ID on congestion(way_id)")
        db.commit()
        self.db = db

    def insertOrUpdate(self, way_id, start_hour, end_hour, weekday, speed, read_count):
        db = self.db
        cursor = db.cursor()
        cursor.execute('''SELECT id, speed, read_count
							FROM congestion
							WHERE way_id={}
							AND start_hour={}
							AND end_hour={}
							AND weekday={}
		'''.format(way_id, start_hour, end_hour, weekday))
        record = cursor.fetchone()
        if (not record):
            cursor.execute('''INSERT INTO congestion(way_id, start_hour, end_hour, weekday, speed, read_count) VALUES({},{},{},{},{},{})'''.format(
                way_id, start_hour, end_hour, weekday, speed, read_count))
        else:
            cursor.execute('''UPDATE congestion SET speed = {}, read_count = {} WHERE id = {}'''.format(speed, read_count, record[0]))
        db.commit()

    def getWaysCongestion(self, hour=None, weekday=None, ways=None):
        db = self.db
        cursor = db.cursor()
        if(hour and weekday):
            query = '''SELECT way_id,start_hour, speed, read_count FROM congestion WHERE weekday = {} AND start_hour <= {} AND end_hour >= {}'''.format(weekday, hour, hour)
        elif hour:
            query = '''SELECT way_id,start_hour, speed, read_count FROM congestion WHERE start_hour <= {} AND end_hour >= {}'''.format(hour, hour)
        elif weekday:
            query = '''SELECT way_id,start_hour, speed, read_count FROM congestion WHERE weekday = {}'''.format(weekday)
        else:
            query = '''SELECT way_id,start_hour, speed, read_count FROM congestion WHERE start_hour = end_hour'''
        
        if ways:
            ways_str = ""
            for idx, way in enumerate(ways):
                ways_str += str(way)
                if idx < len(ways)-1:
                    ways_str += ","
            query += ''' AND way_id in ({})'''.format(ways_str)
        cursor.execute(query)
        records = cursor.fetchall()
        return records
