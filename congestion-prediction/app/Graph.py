import os
import pickle
from osmread import parse_file, Way, Node
from leuvenmapmatching.map.inmem import InMemMap
from annoy import AnnoyIndex
from geojson import Feature, LineString, FeatureCollection


class Graph:
    def __init__(self, osmFilePath, fresh=False):
        nodes = {}
        ways = {}
        cache_path = os.path.join("data", "cache")
        graph_path = os.path.join(cache_path, "graph.dat")
        map_conn_path = os.path.join(cache_path, "hmm.dat")
        annFilePath = os.path.join(cache_path, "nodes.ann")

        ann = AnnoyIndex(2, metric="manhattan")
        map_con = InMemMap("damascus-graph", use_latlon=True, use_rtree=True, index_edges=True, dir=cache_path, deserializing=True)

        if(not fresh and os.path.exists(graph_path) and os.path.exists(annFilePath) and os.path.exists(map_conn_path)):
            self.nodes, self.nodes_keys, self.ways = pickle.load(open(graph_path, "rb"))
            self.map_con = InMemMap.deserialize(pickle.load(open(map_conn_path, "rb")))
            ann.load(annFilePath)
            self.ann = ann
            print("OSM Graph has loaded!")
            return

        if not os.path.exists(cache_path):
            os.makedirs(cache_path)

        print("Start parsing osm file")
        for entity in parse_file(osmFilePath):
            if isinstance(entity, Node):
                nodes[entity.id] = {"location": (entity.lat, entity.lon)}
                map_con.add_node(entity.id, (entity.lat, entity.lon))
            if isinstance(entity, Way) and 'highway' in entity.tags:
                way_name = ""
                if('name' in entity.tags):
                    entity.tags["name"]
                ways[entity.id] = {"name": way_name, "nodes": entity.nodes, "poi": []}
                for node_a, node_b in zip(entity.nodes, entity.nodes[1:]):
                    map_con.add_edge(node_a, node_b)
                    map_con.add_edge(node_b, node_a)
                for node in entity.nodes:
                    nodes[node]["way"] = entity.id

        print("End parsing")
        map_con.purge()
        if(os.path.exists(annFilePath)):
            ann.load(annFilePath)
        else:
            print("Start generating annoy tree")
            for idx, (key, value) in enumerate(nodes.items()):
                v = [value["location"][0], value["location"][1]]
                ann.add_item(idx, v)
            print("Start building annoy tree")
            ann.build(100)
            ann.save(annFilePath)
            print("End")
        self.map_con = map_con
        self.ann = ann
        self.nodes = nodes
        self.nodes_keys = list(nodes.keys())
        self.ways = ways
        pickle.dump((nodes, self.nodes_keys, ways), open(graph_path, "wb"))
        pickle.dump(map_con.serialize(), open(map_conn_path, "wb"))

        print("Finish Building OSM Graph!")

    def getNearestNode(self, lat, lng):
        nearest = self.ann.get_nns_by_vector([lat, lng], 1)[0]
        node_id = self.nodes_keys[nearest]
        return (node_id, self.nodes[node_id])

    def update(self):
        cache_path = os.path.join("data", "cache")
        graph_path = os.path.join(cache_path, "graph.dat")
        pickle.dump((self.nodes, self.nodes_keys, self.ways), open(graph_path, "wb"))

    def getWaysAsGeojson(self, ways_id, props=[]):
        line_strings = []
        ways_id = [int(key) for key in ways_id]
        keys = self.ways.keys()
        idx = 0
        for way_id in ways_id:
            if not way_id in keys:
                print(type(way_id), way_id)
                continue
            way = self.ways[way_id]
            nodes = way["nodes"]
            properties = {
                "name": way.get("name", ""),
                "id": way_id
            }
            if(len(props) > 0):
                properties.update(props[idx])
            line_strings.append(Feature(properties=properties, geometry=LineString([(self.nodes[n_id]["location"][1], self.nodes[n_id]["location"][0]) for n_id in nodes])))
            idx += 1
        return FeatureCollection(line_strings)
