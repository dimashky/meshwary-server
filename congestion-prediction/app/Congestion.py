import dateutil
import sqlite3
import os
import gpxpy
from app.Matcher import hmmMatcher, distanceMatcher
from haversine import haversine
from app.SqlLiteRepo import SqlLiteRepo
from app.Helper import dayPeriod, getColorBasedOnSpeed
from geojson import Feature, LineString, FeatureCollection


class Congestion:
    def __init__(self, graph, sqlite_DB_file='data/congestion.db'):
        self.graph = graph
        sql_dir = os.path.dirname(sqlite_DB_file)
        if not os.path.exists(sql_dir):
            os.makedirs(sql_dir)
        self.sqllite = SqlLiteRepo(sqlite_DB_file)

    def detect(self, logs):
        graph = self.graph
        prev_way = -1
        prev_time = None
        prev_location = None
        speed_dict = {}  # key: way_id, value: (totalspeed, totalcount)
        for node_id, time in logs:
            try:
                if(type(time) == type('string')):
                    time = dateutil.parser.parse(time)
                node_dict = graph.nodes[node_id]
                node_location = node_dict.get("location")
                node_way_id = node_dict.get("way", -1)

                if(prev_way == -1):
                    prev_way = node_dict.get("way", -1)
                    prev_time = time
                    prev_location = node_location
                    continue

                rdelta = dateutil.relativedelta.relativedelta(time, prev_time)
                h = rdelta.hours + (rdelta.minutes/60) + (rdelta.seconds/(60*60))
                km = haversine(node_location, prev_location)
                speed = km/(h + 0.0000001)

                if (h == 0 or km < 0.005 or speed > 120 or speed < 5):
                    continue

                totalspeed, totalread = speed_dict.get(node_way_id, (0, 0))
                speed_dict[node_way_id] = (totalspeed + speed, totalread + 1)

                totalspeed, totalread = speed_dict.get(prev_way, (0, 0))
                speed_dict[prev_way] = (totalspeed + speed, totalread + 1)

                prev_way = node_dict.get("way", -1)
                prev_time = time
                prev_location = node_location
            except Exception as ex:
                print(ex)
        return speed_dict

    def extractCongestionFromGPX(self, filePath, hourly=False, matcher="distance", store=True):
        gpx = gpxpy.parse(open(filePath, 'r'))

        for idx, track in enumerate(gpx.tracks):
            logs = []
            print("read new segement #{}".format(idx))
            for segment in track.segments:
                logs = [(p.latitude, p.longitude, p.time) for p in segment.points]
            matched = []
            print("start matching")
            if(matcher == "hmm"):
                matched = hmmMatcher(self.graph, logs)
            elif(matcher == "distance"):
                matched = distanceMatcher(self.graph, logs)
            print("start detection")
            speeds = self.detect(matched)
            if(len(logs) == 0):
                continue
            dateObj = logs[0][2]
            weekday = dateObj.date().weekday()
            hour = dateObj.time().hour
            start_hour, end_hour = dayPeriod(hour)
            if(hourly):
                start_hour, end_hour = hour, hour
            print("start storing")
            if(not store):
                continue
            for way_id, (totalspeed, totalread) in speeds.items():
                self.sqllite.insertOrUpdate(way_id, start_hour, end_hour, weekday, totalspeed, totalread)

    def getCongestionAsGeojson(self, weekday, hour):
        records = self.sqllite.getWaysCongestion(hour, weekday)
        congestion = {}
        line_strings = []
        graph = self.graph
        for (way_id, start_hour, speed, read_count) in records:
            item = congestion.get(way_id, (0, 0))
            congestion[way_id] = (item[0]+speed, item[1]+read_count)
        for way_id, (speed, cnt) in congestion.items():
            way = self.graph.ways.get(way_id)
            if not way:
                continue
            nodes = way["nodes"]
            avg_speed = speed/(cnt + 0.0000001)
            properties = {
                "name": way.get("name", ""),
                "speed": avg_speed,
                "color": getColorBasedOnSpeed(avg_speed)
            }
            line_strings.append(Feature(properties=properties, geometry=LineString([(graph.nodes[n_id]["location"][1], graph.nodes[n_id]["location"][0]) for n_id in nodes])))
        return FeatureCollection(line_strings)

    def getWaysAvgSpeed(self, ways, weekday, hour):
        historical = self.sqllite.getWaysCongestion(hour, weekday, ways)
        avg_speed = {}
        for way_id, start_hour, speed, read_count in historical:
            value = avg_speed.get(way_id, (0, 0))
            avg_speed[way_id] = (value[0]+speed, value[1]+read_count)
        total_speed, total_count = (0, 0)
        for speed, count in avg_speed.values():
            total_speed += speed
            total_count += count
        return total_speed/(total_count + 0.000000001)
