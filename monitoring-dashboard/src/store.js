import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {},
        accessToken: '',
        server_host: 'http://158.69.211.15:8010',
        nodejs_host: 'http://158.69.211.15:3010',
        socket_host: 'http://158.69.211.15:3010/monitors'
    },
    mutations: {
        UPDATE_USER(state, creeds) {
            state.user = creeds;
        },
        UPDATE_ACCESS_TOKEN(state, accessToken) {
            state.accessToken = accessToken;
            localStorage.setItem('accessToken', accessToken);
        },
        LOGOUT(state) {
            localStorage.setItem('accessToken', '');
            state.accessToken = '';
            state.user = {};
        }
    },
    getters: {
        GET_ACCESS_TOKEN: state => {
            let accessToken = state.accessToken;
            if (accessToken === '') {
                accessToken = localStorage.getItem('accessToken');
            }
            state.accessToken = accessToken;
            return accessToken;
        }
    }
});

