// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import Vuetify from 'vuetify';
import router from './router';
import 'font-awesome/css/font-awesome.css';
import './theme/default.styl';
import VeeValidate from 'vee-validate';
import theme from './theme/colors';
import Truncate from 'lodash.truncate';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import {DataTables, DataTablesServer} from 'vue-data-tables'
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import moment from 'moment';
import store from './store';
import axios from 'axios';
import VueI18n from 'vue-i18n'
import VueHtmlToPaper from 'vue-html-to-paper';

locale.use(lang);
Vue.config.productionTip = false;
Vue.prototype.axios = axios;
Vue.prototype.$store = store;
// Helpers
// Global filters
Vue.filter('truncate', Truncate);
Vue.use(VeeValidate, {fieldsBagName: 'formFields'});
Vue.use(Vuetify, {
    theme,
    options: {
        themeVariations: ['primary', 'secondary', 'accent'],
        extra: {
            mainToolbar: {
                color: 'primary',
            },
            sideToolbar: {},
            sideNav: 'primary',
            mainNav: 'primary lighten-1',
            bodyBg: '',
        }
    }
});

Vue.use(moment);
Vue.use(ElementUI);
Vue.use(DataTables);
Vue.use(DataTablesServer);

window.moment = moment;
window.axios = axios;

axios.defaults.baseURL = store.state.server_host + '/api/';
axios.defaults.headers.Authorization = 'Bearer ' + store.getters.GET_ACCESS_TOKEN;
axios.defaults.headers.Accept = 'application/json';

new Vue({
    el: '#app',
    router,
    store,
    components: {App},
    template: '<App/>'
});
