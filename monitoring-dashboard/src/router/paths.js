import ErrorPaths from './Error.js';
import store from '../store';
import axios from 'axios';

let permissionGuard = (to, from, next) => {
    if (store.state.user.permission == 1) {
        next();
    } else {
        next({name: 'DriverTracking'});
    }
};

export default [
    ...ErrorPaths,
    {
        path: '/login',
        name: 'NOW_LOGIN',
        beforeEnter(to, from, next) {
            axios.get('user')
                .then(res => {
                    store.commit('UPDATE_USER', res.data);
                    next('DriverTracking');
                })
                .catch(err => {
                    next();
                });
        },
        component: () => import(
            `@/pages/Login.vue`
            )
    },
    {
        path: '/map',
        name: 'Map',
        beforeEnter: async (to, from, next) => {
            axios.get('user')
                .then(res => {
                    store.commit('UPDATE_USER', res.data);
                    next();
                }).catch(err => {
                console.log(err.message);
                next('Login');
            });
        },
        component: () => import(
            `@/pages/Map.vue`
            )
    },
    {
        path: '/path_drawer',
        name: 'PathDrawer',
        beforeEnter: async (to, from, next) => {
            axios.get('user')
                .then(res => {
                    store.commit('UPDATE_USER', res.data);
                    next();
                }).catch(err => {
                console.log(err.message);
                next('Login');
            });
        },
        component: () => import(
            `@/pages/PathDrawer.vue`
            )
    },
    {
        path: '/',
        meta: {},
        name: 'Root',
        component: require('../pages/CMS').default,
        redirect: {
            name: 'PathDrawer'
        },
        beforeEnter: async (to, from, next) => {
            axios.get('user')
                .then(res => {
                    store.commit('UPDATE_USER', res.data);
                    next();
                }).catch(err => {
                console.log(err.message);
                next('Login');
            });
        },
        children: [
            // {
            //     path: '/path_drawer',
            //     meta: {breadcrumb: true},
            //     name: 'PathDrawer',
            //     component: () => import(
            //         `@/pages/PathDrawer.vue`
            //         )
            // }
        ]
    },
    {
        path: '*',
        meta: {
            public: true,
        },
        redirect: {
            path: '/404'
        }
    },
];
