'use strict'

const {ServiceProvider} = require('@adonisjs/fold');
const RPC = require('./../app/Services/RPC');

class RPCServiceProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
      this.app.singleton('RPC', () => {
          return new RPC(use('Env'));
      });
  }
}

module.exports = RPCServiceProvider
