'use strict'

const {ServiceProvider} = require('@adonisjs/fold');
const MongoDB = require('./../app/Services/MongoDB');

class MongoDbServiceProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    this.app.singleton('MongoDB', () => {
      return new MongoDB(use('Env'));
    })
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    //
  }
}

module.exports = MongoDbServiceProvider
