'use strict';

const {ServiceProvider} = require('@adonisjs/fold');
const SocketIO = require('./../app/Services/SocketIO');

class SocketIoServiceProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    //
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    this.app.singleton('SocketIO', () => {
      return new SocketIO(use('Server').getInstance());
    });
  }
}

module.exports = SocketIoServiceProvider;
