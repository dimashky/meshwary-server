'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/api/test', async ({request}) => {
    const rpc = use('RPC');
    const moment = require('moment');
    rpc.start(moment(request.datetime).locale('en').format('YYYY-MM-DD HH:mm:ss'), request.seconds);
    return 'success';
});


// Route.post('/api/start', 'App/Controllers/CollectorController.startLine').middleware('custom_auth');

// Route.put('/api/end', 'App/Controllers/CollectorController.endLine').middleware('custom_auth');

// Route.post('/api/stop', 'App/Controllers/CollectorController.newStop').middleware('custom_auth');

// Route.get('/api/active_route', 'App/Controllers/CollectorController.activeRoute').middleware('custom_auth');

Route.get('/api/lines', async ({request, response}) => {
    const mongodb = use('MongoDB');
    let data = null;
    await mongodb.Lines.find({}, {route: 0}).then(res => data = res);
    return response.send({data: data});
});

Route.get('/api/lines/route', async ({request, response}) => {
    const mongodb = use('MongoDB');
    let data = null;
    await mongodb.Lines.find({_id: request.get('id').id}, {route: 1}).then(res => data = res[0]).catch(err => data = []);
    return response.send({data: data});
});
