const driverController = use('App/Controllers/DriverController');
const collectorController = use('App/Controllers/CollectorController');
const passengerController = use('App/Controllers/PassengerController');
const auth = use('App/Middleware/Socket/Auth');
const io = use('SocketIO').io;

io.of('drivers').use(auth).on('connection', (socket) => {
    console.log("new driver connected with Socket ID: ", socket.id);
    driverController.connect(socket);
    socket.on('update_location', (data) => driverController.updateLocation(socket.id, data));
    socket.on('disconnect', () => driverController.disconnect(socket.id));
});

io.of('collectors').use(auth).on('connection', (socket) => {
    collectorController.connect(socket);
    socket.on('update_location', (data) => collectorController.updateLocation(socket, data));
    socket.on('disconnect', () => collectorController.disconnect(socket.id));
});


io.of('monitors').use(auth).on('connection', (socket) => {
    console.log('new monitor connected');
    socket.on('disconnect', () => console.log('monitor disconnected'));
});

io.of('passengers').use(auth).on('connection', socket => {
    console.log('new passenger connected');
    socket.on('update_location', (data) => passengerController.updateLocation(socket, data));

    socket.on('disconnect', () => console.log('monitor disconnected'));
});


// CLIENT
const socket_client_io = require('socket.io-client');
const moment = require('moment');
const mongo = use('MongoDB');
require('moment/locale/ar');

const socket_client = socket_client_io('http://stgnode.wasilni.com/users', {
    transportOptions: {
        polling: {
            extraHeaders: {
                authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZlZDUyZjBkYjY2MDdiMzBkZGU3YzY3OWUyODZmMTRlYzlkZmZhMThkNTI2YTg5Y2ZlMDgzOTllZWRjZTU2OTc4MGIxZTY1YTc3MGE3MmUyIn0.eyJhdWQiOiI1IiwianRpIjoiNmVkNTJmMGRiNjYwN2IzMGRkZTdjNjc5ZTI4NmYxNGVjOWRmZmExOGQ1MjZhODljZmUwODM5OWVlZGNlNTY5NzgwYjFlNjVhNzcwYTcyZTIiLCJpYXQiOjE1NzM0MTY5MjUsIm5iZiI6MTU3MzQxNjkyNSwiZXhwIjoxNjA1MDM5MzI1LCJzdWIiOiIyMyIsInNjb3BlcyI6W119.LK9OoXzTtpdCgPGERCZXPcvUsDbXI7gSF8d7xvnjedy_fool8Hd3tWRmNCn6h3Z4rpap8QAzfJ-HG32kCfWkCLM3fY0E4gRp15rVItWkE5fEHu5Xe9ofOz1Q5_z5mYiikzrZdztVpbzbeemrLWFBo-tgfoedOkNrZuyLJpvdjteQb5urj5Jncfpfo-vBpq9yJfmIm98zcLLw_QM0IWZpMru_x3UXSkY3BmUPM25awoPSJlwLkMXdN1FfvqhtMHu224PHVxW4SS51nPMs72UqS4zfkvMVTs67qXFxLx8ZJQY8-WbaHEHZQ-_DIwK013EcO7vMGHvU4QlRWcdR1h6VO7gTpBH8b7N9vrffttyPwtUl0U8asf494e4BD1SVp-a8KpHRSCPoN4fV3Pnk4wJdhc9axKMoORDFzkCs2UN7_Ic79biedWxZY4nl9EEbiq9By9uogwiWsNMQiUl7NbfgdS9hvvp0uwoGWGySHFk315QpXgePh0xTtK5mVXCherac-d9D7wWJcspbSlEEo1UwZXiRaAyTS6f4-oVl084l23J3DIp7zWdbxYno0Mc9ChKrDnbF6_eujCoLxw15r_1NG6TMG0d6b3HRQk8Q6WN48OS4rNxKYeAR_hqvL0KhFGzDEnCV69NHCuEDHKkg71NsrGcicze3nT3kODxy5FwLr4c'
            }
        }
    }
});

socket_client.on('captain_location_updated', (data) => {
    data.last_seen = moment(data.last_seen).locale('en').format('YYYY-MM-DD HH:mm:ss');
    data.created_at = moment().locale('en').format('YYYY-MM-DD HH:mm:ss');
    mongo.RealTime.create(data);
});


// run RPC

const rpc = use('RPC');


rpc.start(moment('2019-06-30 13:00:00').locale('en').format('YYYY-MM-DD HH:mm:ss'), 10);

