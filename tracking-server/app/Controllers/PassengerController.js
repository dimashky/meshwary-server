const moment = use('moment');

const mongodb = use('MongoDB');

const IO = use('SocketIO').io;

require('moment/locale/ar');

const geoLib = require('geolib');


class PassengerController {
    static connect(socket) {
        console.log('passenger connected ' + socket.name);
    }

    static disconnect(socket) {
        console.log('passenger Disconnected ' + socket.name);
    }

    static updateLocation(socket, data) {
        data.date = moment(data.date).locale('en').format('YYYY-MM-DD HH:mm:ss');
        console.log('passenger ' + socket.name + ' has update his location ' + data.date + ` ${data.lat} ${data.lng}`);

        let sent_data = JSON.parse(JSON.stringify(data));
        sent_data.name = socket.name;
        sent_data.id = socket.user_id;
        sent_data.type = 'PASSENGER';
        IO.of('monitors').emit('location_updated', sent_data);
        mongodb.Trips.find({user_id: socket.user_id, end_time: null})
            .then(res => {
                res.forEach(elem => {
                    let new_distance = 0;
                    let avg = 0;
                    if (elem.route.length > 0) {
                        new_distance = geoLib.getDistance(
                            {
                                latitude: elem.route[elem.route.length - 1].lat,
                                longitude: elem.route[elem.route.length - 1].lng
                            },
                            {latitude: data.lat, longitude: data.lng},
                        ) / 1000000.0 + (elem.distance ? elem.distance : 0);
                        avg = geoLib.getSpeed(
                            {
                                latitude: elem.route[0].lat,
                                longitude: elem.route[0].lat,
                                time: moment(elem.route[0].date).locale('en').valueOf()
                            },
                            {latitude: data.lat, longitude: data.lat, time: moment(data.date).locale('en').valueOf()});
                        avg = geoLib.convertSpeed(avg, 'kmh');
                    }
                    console.log(new_distance, avg);
                    mongodb.Trips.update({_id: elem._id}, {
                        $set: {distance: new_distance, avg: avg},
                        $push: {route: data},
                    }, {strict: false})
                        .then(() => console.log('UPDATED'))
                        .catch(err => console.error(err.message));
                });
            }).catch(err => console.log(err.message));
    }
}

module.exports = PassengerController;
