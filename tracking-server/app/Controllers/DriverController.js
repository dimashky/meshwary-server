const moment = use('moment');

const mongodb = use('MongoDB');

const IO = use('SocketIO').io;

require('moment/locale/ar');

class DriverController {
    static connect(socket) {
        mongodb.BusSession.create({
            session_id: socket.id,
            bus_id: socket.user_id
        });
    }

    static disconnect(socket_id) {
        console.log('user Disconnected');
        mongodb.BusSession.update(
            {session_id: socket_id},
            {$set: {end_date: moment().locale('en').format('YYYY-MM-DD HH:mm:ss')}}
        ).then(res => console.log(res)).catch(err => console.log(err.message));
    }

    static updateLocation(socket_id, data) {
        data.date = moment(data.date).locale('en').format('YYYY-MM-DD HH:mm:ss');
        data.type = 'DRIVER';
        IO.emit('location_updated', data);
        mongodb.BusSession.update(
            {session_id: socket_id},
            {$push: {route: data}}
        ).then(res => console.log(res)).catch(err => console.log(err.message));
    }
}

module.exports = DriverController;
