const moment = use('moment');

const mongodb = use('MongoDB');

const IO = use('SocketIO').io;

require('moment/locale/ar');

class CollectorController {
    static connect(socket) {
        console.log('collector connected ' + socket.name);
    }

    static disconnect(socket) {
        console.log('collector Disconnected ' + socket.name);
    }

    static updateLocation(socket, data) {
        data.date = moment(data.date).locale('en').format('YYYY-MM-DD HH:mm:ss');
        console.log('Collector ' + socket.name + ' has update his location ' + data.date +  ` ${data.lat} ${data.lng}`);

        let sent_data = JSON.parse(JSON.stringify(data));
        sent_data.name = socket.name;
        sent_data.id = socket.user_id;
        sent_data.type = 'COLLECTOR';
        IO.of('monitors').emit('location_updated', sent_data);

        mongodb.Lines.update(
            {bus_id: socket.user_id, can_update: true},
            {$push: {route: data}}
        ).then(res => console.log(res)).catch(err => console.log(err.message));
    }

    // startLine({request, response}) {
    //     try {
    //         mongodb.Lines.update(
    //             {bus_id: request.user_id},
    //             {can_update: false},
    //             {multi: true}
    //         ).then(res => console.log(res)).catch(err => console.log(err.message));
    //         mongodb.Lines.create({
    //             bus_id: request.user_id,
    //             name: request.all().name
    //         });
    //         return response.send({message: 'success'});
    //     } catch (e) {
    //         return response.status(400).send({message: e.message});
    //     }
    // }

    // endLine({request, response}) {
    //     try {
    //         mongodb.Lines.update(
    //             {bus_id: request.user_id},
    //             {$set: {can_update: false}},
    //             {multi: true}
    //         ).then(res => console.log(res)).catch(err => console.log(err.message));
    //         return response.send({message: 'success'});
    //     } catch (e) {
    //         return response.status(400).send({message: e.message});
    //     }
    // }

    // newStop({request, response}) {
    //     try {
    //         const stop = {
    //             lat: request.all().lat,
    //             lng: request.all().lng,
    //             date: moment().locale('en').format("YYYY-MM-DD HH:mm:ss")
    //         };
    //         mongodb.Lines.update(
    //             {bus_id: request.user_id, can_update: true},
    //             {$push: {stops: stop}}
    //         ).then(res => console.log(res)).catch(err => console.log(err.message));
    //         return response.send({message: 'success'});
    //     } catch (e) {
    //         return response.status(400).send({message: e.message});
    //     }
    // }

    // async activeRoute({request, response}) {
    //     try {
    //         let data = [];
    //         await mongodb.Lines.find({bus_id: request.user_id})
    //             .then(res => {
    //                 data = res;
    //             }).catch(err => console.log(err.message));
    //         return response.send({message: 'success', data: data});
    //     } catch (e) {
    //         return response.status(400).send({message: e.message});
    //     }
    // }
}

module.exports = CollectorController;
