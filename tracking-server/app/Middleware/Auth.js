'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const axios = use('axios');

class Auth {
    /**
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Function} next
     */

    async handle({request, response}, next) {
        const Env = use('Env');

        await axios.get(`http://${Env.get('MAIN_SERVER_HOST', '127.0.0.1')}:${Env.get('MAIN_SERVER_PORT', 8000)}/api/user`, {headers: {Authorization:  request.header('Authorization')}})
            .then(async res => {
                request['user_id'] = res.data.id;
                console.log(request.user_id);
                console.log('successfully authorized');
                await next();
            })
            .catch(async err => {
                console.log('ERROR', err);
                return response.status(401).send('Unauthorized');
            });
    }
}

module.exports = Auth;
