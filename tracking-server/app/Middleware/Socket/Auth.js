const axios = use('axios');

module.exports = async (socket, next) => {

    let authorization = '';
    if (socket.handshake.headers.authorization) {
        authorization = socket.handshake.headers.authorization;
    } else {
        authorization = socket.handshake.query.authorization;
    }

    const headers = {
        'Accept': 'application/json',
        'Authorization': authorization
    };

    console.log("NEW CONNECTION " + socket.nsp.name);
    console.log(headers);
    /*
    * TODO: disconnect last Socket if exist
    * */
    const Env = use('Env');
    if(socket.nsp.name === '/monitors') {
        console.log('wooow');
        return next();
    }

    await axios.get(`http://${Env.get('MAIN_SERVER_HOST', '127.0.0.1')}:${Env.get('MAIN_SERVER_PORT', 8000)}/api/user`, {headers: headers})
        .then(async res => {
            socket['user_id'] = res.data.id;
            socket['name'] = res.data.name;
            console.log(socket.user_id);
            console.log('successfully authorized');
            // res.data.socket_id = Socket.id;
            // res.data.authorization = Socket.handshake.headers.authorization;
            await next();
        })
        .catch(async err => {
            console.log(err);
            await next(new Error(err));
        });
};
