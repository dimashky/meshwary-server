const mongoose = use('mongoose');

class MongoDB {
    constructor(Env) {
        this.Env = Env;
        const connection = `mongodb://${this.Env.get('DB_DATABASE_USERNAME', '')}:${this.Env.get('DB_DATABASE_PASSWORD', '')}@${this.Env.get('DB_HOST_MONGO', 'localhost')}:${this.Env.get('DB_PORT_MONGO', '27017')}/${this.Env.get('DB_DATABASE_MONGO', 'WasilniRealTime')}`;

        mongoose.connect(connection, {poolSize: 10, useNewUrlParser: true})
            .then((db) => {
                this.db = db;
            }).catch(err => console.error.bind(console, 'MongoDB connection error', err));
        this.BusSession = mongoose.model('BusSession', this.BusSessionSchema());
        this.Lines = mongoose.model('Lines', this.LinesSchema());
        this.Trips = mongoose.model('Trips', this.TripsSchema());
        this.RealTime = mongoose.model('real_time', this.RealTimeSchema());
    }

    BusSessionSchema() {
        let schema = new mongoose.Schema({
            session_id: {
                type: String,
                index: true,
                unique: true
            },
            bus_id: {
                type: Number,
                index: true
            },
            route: {type: [{lat: Number, lng: Number, date: Date}], default: []},
            start_date: {type: Date, default: Date.now},
            end_date: {type: Date, default: null}
        });
        schema.index({session_id: 1});
        return schema;
    }

    LinesSchema() {
        let schema = new mongoose.Schema({
            bus_id: {
                type: Number,
                index: true
            },
            name: {type: String, default: 'no name given'},
            route: [],
            stops: {type: [{lat: Number, lng: Number, date: Date}], default: []},
            can_update: {type: Boolean, default: true}
        });
        schema.index({session_id: 1});
        return schema;
    }

    TripsSchema() {
        let schema = new mongoose.Schema({
            user_id: {
                type: Number,
                index: true
            },
            route: [],
            start_time: {type: Date},
            end_time: {type: Date, default: null},
            distance: {type: Number, default: 0.0},
            avg: {type: Number, default: 0.0}
        });
        schema.index({session_id: 1});
        return schema;
    }

    RealTimeSchema() {
        return new mongoose.Schema({
            id: {type: Number},
            location: {type: Object},
            last_seen: {type: Date},
            created_at: {type: Date}
        }, {strict: false});
    }


}

module.exports = MongoDB;
