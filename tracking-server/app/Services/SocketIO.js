class SocketIO {
    constructor(Server) {
        this.io = use('socket.io')(Server, {
            handlePreflightRequest: function (req, res) {
                res.writeHead(200, {
                    'Access-Control-Allow-Headers': 'Content-Type, Authorization',
                    'Access-Control-Allow-Origin': req.headers.origin,
                    'Access-Control-Allow-Credentials': true
                });
                res.end();
            }
        });
    }
}

module.exports = SocketIO;
