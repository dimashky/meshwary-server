const zerorpc = require('zerorpc');
const moment = require('moment');

const redis  = require('redis');
client = redis.createClient();
client.on("error", function (err) {
    console.log("Error " + err);
});

class RPC {
    constructor(Env) {
        try {
            this.client = new zerorpc.Client();
            this.client.connect(`tcp://${Env.get('RPC_ADDRESS')}`, '');
            this.interval = null;
        } catch (e) {
            console.log('RPC ERROR');
            console.log(e.message);
        }
    }

    start(start_time, second_separator) {
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.time = moment.utc(start_time).locale('en');
        this.second_separator = second_separator;
        this.tracker();
        this.startInterval();
    }

    startInterval() {
        return setInterval(() => {
            // no need for this line if tracker is running else you need to uncomment it
            // this.time.add(this.second_separator, 'seconds');
            const mongo = use('MongoDB');
            const socket = use('SocketIO').io;

            mongo.RealTime.find({
                last_seen: {
                    $gte: this.time.clone().subtract(3, 'minutes').format(),
                    $lte: this.time.format()
                }
            }).then(res => {
                let arr = [];
                let map = new Map();
                res.forEach(e => {
                    if (map.has(e.id)) {
                        arr[map.get(e.id)].push([e.lat, e.lng, e.last_seen.toISOString()]);
                    } else {
                        arr.push([[e.lat, e.lng, e.last_seen.toISOString()]]);
                        map.set(e.id, arr.length - 1);
                    }
                });
                console.log(arr.length);
                arr.forEach(e => console.log(e.length));
                this.client.invoke("pushTrackingLogs", arr, function (error, res) {
                    try {
                        console.log(res['congestion']);
                        socket.of('monitors').emit('congestion_updated', res['congestion']);
                        if (error) {
                            console.log(error);
                        }
                    } catch (e) {
                        console.log(e.message);
                    }
                });
            }).catch(err => console.log(err.message));
        }, 1000 * this.second_separator);
    }

    tracker() {
        return setInterval(() => {
            this.time.add(1, 'seconds');
            const mongo = use('MongoDB');
            const socket = use('SocketIO').io;
            mongo.RealTime.find({
                last_seen: this.time.format()
            }).then(res => {
                console.log('socket emit', res.length);
                res.forEach(e => {
                    let x = e.toJSON();
                    x.name = 'Vehicle ' + x.id;
                    x.date = x.last_seen;
                    socket.of('monitors').emit('location_updated', x);
                });
            }).catch(err => console.log(err.message));
        }, 1000);
    }

    getTime() {
        return this.time.locale('en').format('YYYY-MM-DD HH:mm:ss');
    }
}

module.exports = RPC;
