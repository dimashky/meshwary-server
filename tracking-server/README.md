# Meshwary

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

1- Initialize `.env` file.

2- run following command `npm install`.

### Running the Server

`adonis serve --dev`

